from .settings import Settings

settings = Settings()
as_url = settings.as_url.rstrip("/")
mds_url = settings.mds_url.rstrip("/")
mpsm_url = settings.mpsm_url.rstrip("/")
