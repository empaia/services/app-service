import pytest
import requests

from ..singletons import as_url, mds_url
from .helper import create_portal_app_and_running_job


def test_set_progress():
    job_token = create_portal_app_and_running_job(
        io_spec={},
        modes_spec={
            "standalone": {
                "inputs": [],
                "outputs": [],
            },
        },
        inputs={},
    )
    job_id = job_token["job_id"]
    access_token = job_token["access_token"]
    headers = {"Authorization": f"Bearer {access_token}"}

    # update job progress
    url = f"{as_url}/v3/{job_id}/progress"
    response = requests.put(url, headers=headers, json={"progress": 0.75}, timeout=5)
    assert response.status_code == 200

    # ensure it was recognized internally
    url = f"{mds_url}/v3/jobs/{job_id}"
    response = requests.get(url, timeout=5)
    assert response.status_code == 200
    job = response.json()
    assert job["progress"] == pytest.approx(0.75)
