import json
import uuid

import requests

from ..singletons import mds_url, mpsm_url


def create_portal_app_and_running_job(
    io_spec, modes_spec, inputs, permissions_spec=None, mode="STANDALONE", config_spec=None
):
    app_id = create_portal_app(io_spec, modes_spec, permissions_spec, config_spec)
    return create_running_job(app_id, mode, inputs, io_spec)


def create_portal_app(io_spec, modes_spec, permissions_spec, config_spec):
    ead = _generate_ead(io_spec, modes_spec, permissions_spec, config_spec)
    portal_app, app_id = _generate_portal_app(ead)
    url = f"{mpsm_url}/v1/custom-mock/portal-apps"
    response = requests.post(url, json=portal_app, timeout=5)
    print(response.status_code, response.text)
    assert response.status_code == 200
    return app_id


def create_running_job(app_id, mode, inputs, io_spec=None):
    creator_id = str(uuid.uuid4())
    job = {
        "app_id": app_id,
        "creator_id": creator_id,
        "creator_type": "SERVICE" if mode == "PREPROCESSING" else "SCOPE",
        "mode": mode,
        "containerized": True,
    }
    url = f"{mds_url}/v3/jobs"
    response = requests.post(url, json=job, timeout=5)
    assert response.status_code == 200
    job = response.json()
    job_id = job["id"]
    for input_key, input_payload in inputs.items():
        _create_job_input(job_id, input_key, input_payload, creator_id)
        if input_payload["type"] == "fhir_questionnaire":
            _create_selector_value(job_id, input_key, io_spec[input_key]["selectors"][0])
    _set_job_running(job_id)
    url = f"{mds_url}/v3/jobs/{job_id}/token"
    response = requests.get(url, timeout=5)
    assert response.status_code == 200
    return response.json()


def add_job_to_open_examination(job_id, case_id):
    url = f"{mds_url}/v3/jobs/{job_id}"
    response = requests.get(url, timeout=5)
    assert response.status_code == 200
    job = response.json()
    app_id = job["app_id"]
    examination = {"case_id": case_id, "app_id": app_id, "creator_id": str(uuid.uuid4()), "creator_type": "USER"}
    url = f"{mds_url}/v3/examinations"
    response = requests.put(url, json=examination, timeout=5)
    assert response.status_code == 200 or response.status_code == 201
    examination = response.json()
    job_id = job["id"]
    examination_id = examination["id"]
    url = f"{mds_url}/v3/examinations/{examination_id}/jobs/{job_id}/add"
    response = requests.put(url, timeout=5)
    assert response.status_code == 201


def create_slide_in_case(storage_address):
    case_id = create_case()
    slide_id = create_slide(case_id)
    create_storage(storage_address, slide_id)
    return slide_id


def create_case():
    case_ = {
        "creator_id": str(uuid.uuid4()),
        "creator_type": "USER",
    }
    url = f"{mds_url}/private/v3/cases"
    response = requests.post(url, json=case_, timeout=5)
    assert response.status_code == 200
    case_ = response.json()
    return case_["id"]


def create_slide(case_id):
    slide = {"case_id": case_id}
    url = f"{mds_url}/private/v3/slides"
    response = requests.post(url, json=slide, timeout=5)
    assert response.status_code == 200
    slide = response.json()
    return slide["id"]


def create_storage(storage_adress, slide_id):
    url = f"{mds_url}/private/v3/slides/{slide_id}/storage"
    slide_storage_info = {"main_storage_address": {"storage_address_id": str(uuid.uuid4()), "path": storage_adress}}
    r = requests.put(url, data=json.dumps(slide_storage_info), timeout=5)
    print(r.content)
    assert r.status_code == 200


def update_slide(slide_id, tissue, stain):
    update = {
        "tissue": tissue,
        "stain": stain,
    }
    url = f"{mds_url}/private/v3/slides/{slide_id}"
    response = requests.put(url, json=update, timeout=5)
    assert response.status_code == 200


def add_global_app_config(app_id, config):
    url = f"{mpsm_url}/v1/custom-mock/apps/{app_id}/config/global"
    response = requests.put(url, json=config, timeout=5)
    assert response.status_code == 200


def add_customer_app_config(app_id, config, customer_id):
    url = f"{mpsm_url}/v1/custom-mock/apps/{app_id}/config/customer/{customer_id}"
    response = requests.put(url, json=config, timeout=5)
    assert response.status_code == 200


def _create_job_input(job_id, input_key, payload, creator_id):
    resource = _create_resource(payload, creator_id)
    _set_job_input(job_id, input_key, resource["id"])


def _create_resource(payload, creator_id):
    _set_creator_id_and_type(payload, creator_id)
    if payload["type"] == "wsi":
        return {"id": payload["slide_id"]}
    elif payload["type"] in ("integer", "float", "bool", "string"):
        return _create_primitive(payload)
    elif payload["type"] in ("line", "arrow", "rectangle", "point", "circle", "polygon"):
        return _create_annotation(payload)
    elif payload["type"] == "collection":
        return _create_collection(payload)
    elif payload["type"] == "fhir_questionnaire":
        return {"id": payload["fhir_questionnaire_id"]}
    else:
        raise NotImplementedError()


def _set_creator_id_and_type(payload, creator_id):
    if payload["type"] == "collection":
        for item in payload["items"]:
            _set_creator_id_and_type(item, creator_id)
    if payload["type"] != "wsi":
        payload["creator_id"] = creator_id
        payload["creator_type"] = "scope"


def _create_primitive(payload):
    url = f"{mds_url}/v3/primitives"
    return _post_payload(url, payload)


def _create_annotation(payload):
    url = f"{mds_url}/v3/annotations"
    return _post_payload(url, payload)


def _create_collection(payload):
    url = f"{mds_url}/v3/collections"
    return _post_payload(url, payload)


def _post_payload(url, payload):
    response = requests.post(url, json=payload, timeout=5)
    assert response.status_code == 201
    return response.json()


def _set_job_input(job_id, input_key, input_id):
    url = f"{mds_url}/v3/jobs/{job_id}/inputs/{input_key}"
    response = requests.put(url, json={"id": input_id}, timeout=5)
    assert response.status_code == 200


def _set_job_running(job_id):
    url = f"{mds_url}/v3/jobs/{job_id}/status"
    response = requests.put(url, json={"status": "RUNNING"}, timeout=5)
    assert response.status_code == 200


def _generate_portal_app(ead):
    creator_id = str(uuid.uuid4())
    organization_id = str(uuid.uuid4())
    portal_app_id = str(uuid.uuid4())
    app_view_id = str(uuid.uuid4())
    app_id = str(uuid.uuid4())
    app = {
        "id": portal_app_id,
        "organization_id": organization_id,
        "status": "DRAFT",
        "active_app_views": {
            "v3": {
                "api_version": "v3",
                "version": "v1.2",
                "details": {
                    "name": "PD-L1 Quantifier",
                    "marketplace_url": "http://url.to/store",
                    "description": [{"lang": "EN", "text": "Some text"}],
                },
                "media": {"peek": [], "banner": [], "workflow": [], "manual": []},
                "tags": {"tissues": [], "stains": [], "indications": [], "analysis": []},
                "non_functional": False,
                "created_at": "1598611645",
                "reviewed_at": "1598611645",
                "id": app_view_id,
                "portal_app_id": portal_app_id,
                "organization_id": organization_id,
                "status": "DRAFT",
                "app": {
                    "ead": ead,
                    "registry_image_url": "https://registry.gitlab.com/empaia/integration/ap_xyz",
                    "app_ui_url": "http://app1.emapaia.org",
                    "app_ui_configuration": {"csp": None},
                    "id": app_id,
                    "version": "v1.2",
                    "has_frontend": True,
                    "status": "DRAFT",
                    "portal_app_id": portal_app_id,
                    "creator_id": creator_id,
                    "created_at": "1598611645",
                    "updated_at": "1598611645",
                },
                "creator_id": creator_id,
                "review_comment": "Review comment",
                "reviewer_id": creator_id,
            },
        },
        "creator_id": creator_id,
        "created_at": "1598611645",
        "updated_at": "1598611645",
    }
    return app, app_id


def _generate_ead(io_spec, modes_spec, permissions_spec=None, config=None):
    ead = {
        "$schema": "https://gitlab.com/empaia/integration/definitions/-/raw/main/ead/ead-schema.v3.json",
        "name": "Test App",
        "name_short": "TestApp",
        "namespace": "org.empaia.vendor_name.ta.v3.1",
        "description": "EAD for testing purposes",
        "io": io_spec,
        "modes": modes_spec,
    }
    if permissions_spec:
        ead["permissions"] = permissions_spec
    if config:
        ead["configuration"] = config
    return ead


def create_questionnaire():
    questionnaire_resource = {"resourceType": "Questionnaire", "status": "active"}
    url = f"{mds_url}/v3/fhir/questionnaires"
    return _post_payload(url, questionnaire_resource)


def _create_selector_value(job_id, input_key, selector_value):
    response = requests.put(
        f"{mds_url}/v3/jobs/{job_id}/inputs/{input_key}/selector",
        json={"selector_value": f"{selector_value}"},
        timeout=60,
    )
    assert response.status_code == 200
