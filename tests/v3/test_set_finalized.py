import requests

from ..singletons import as_url, mds_url
from .helper import create_portal_app_and_running_job


def test_set_finalized():
    job_token = create_portal_app_and_running_job(
        io_spec={
            "my_primitive": {"type": "integer"},
        },
        modes_spec={
            "standalone": {
                "inputs": [],
                "outputs": ["my_primitive"],
            },
        },
        inputs={},
    )
    job_id = job_token["job_id"]
    access_token = job_token["access_token"]
    headers = {"Authorization": f"Bearer {access_token}"}

    # finalization of app not possible with missing outputs
    url = f"{as_url}/v3/{job_id}/finalize"
    response = requests.put(url, headers=headers, timeout=5)
    assert response.status_code == 409

    # set missing output
    my_primitive = {
        "name": "My Integer",
        "type": "integer",
        "creator_id": job_id,
        "creator_type": "job",
        "value": 42,
    }
    url = f"{as_url}/v3/{job_id}/outputs/my_primitive"
    response = requests.post(url, headers=headers, json=my_primitive, timeout=5)
    assert response.status_code == 200
    my_primitive = response.json()

    # finalization of app now possible
    url = f"{as_url}/v3/{job_id}/finalize"
    response = requests.put(url, headers=headers, timeout=5)
    assert response.status_code == 200

    # ensure finalization was recognized internally
    url = f"{mds_url}/v3/jobs/{job_id}"
    response = requests.get(url, timeout=5)
    assert response.status_code == 200
    job = response.json()
    assert job["status"] == "COMPLETED"

    # ensure output has been locked internally
    # query for primitive_id and job_id
    # response must be exactly 1 element if locking was successful
    # primitive is only returned if it was locked for specified job_id in the annotation service
    my_primitive_id = my_primitive["id"]
    url = f"{mds_url}/v3/primitives/query"
    query = {"primitives": [my_primitive_id], "jobs": [job_id]}
    response = requests.put(url, json=query, timeout=5)
    assert response.status_code == 200
    data = response.json()
    assert data["item_count"] == 1
    assert data["items"][0]["id"] == my_primitive_id

    # prevent from promoting finalization again
    url = f"{as_url}/v3/{job_id}/finalize"
    response = requests.put(url, headers=headers, timeout=5)
    assert response.status_code == 400
