import requests

from ..singletons import as_url
from .helper import (
    create_portal_app_and_running_job,
    create_questionnaire,
    create_slide_in_case,
)


def test_get_fhir_questionnaire():
    slide_id = create_slide_in_case("Aperio/CMU-1.svs")
    questionnaire_resource = create_questionnaire()
    input_key = "my_questionnaire"
    selectors = "org.empaia.my_vendor.my_app.v3.1.selectors.something.something-else"
    job_token = create_portal_app_and_running_job(
        io_spec={
            "my_slide": {"type": "wsi"},
            input_key: {
                "type": "fhir_questionnaire",
                "selectors": [f"{selectors}"],
            },
            "my_questionnaire_response": {"type": "fhir_questionnaire_response"},
        },
        modes_spec={
            "preprocessing": {
                "inputs": ["my_slide", input_key],
                "outputs": ["my_questionnaire_response"],
            },
        },
        inputs={
            "my_slide": {
                "type": "wsi",
                "slide_id": slide_id,
            },
            input_key: {
                "type": "fhir_questionnaire",
                "fhir_questionnaire_id": (
                    f'{questionnaire_resource["id"]}||{questionnaire_resource["meta"]["versionId"]}'
                ),
            },
        },
        mode="PREPROCESSING",
    )
    job_id = job_token["job_id"]
    access_token = job_token["access_token"]
    headers = {"Authorization": f"Bearer {access_token}"}

    url = f"{as_url}/v3/{job_id}/inputs/{input_key}"
    res = requests.get(url, headers=headers, timeout=5)
    assert res.status_code == 200
    res_questionnaire = res.json()
    assert res_questionnaire["resourceType"] == "Questionnaire"
    assert "id" in res_questionnaire
    assert res_questionnaire["status"] == "active"
    assert "meta" in res_questionnaire
    assert "versionId" in res_questionnaire["meta"]
    print(res_questionnaire["id"])

    # GET selector_value
    url = f"{as_url}/v3/{job_id}/inputs/{input_key}/selector"
    response = requests.get(url, headers=headers, timeout=5)
    assert response.status_code == 200
    selector_value_result = response.json()
    assert selector_value_result["id"] == job_id
    assert selector_value_result["input_key"] == input_key
    assert selector_value_result["selector_value"] == selectors
