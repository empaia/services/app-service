from random import randint

import requests

from ..singletons import as_url
from .helper import create_portal_app_and_running_job, create_slide_in_case


def test_extend_collection_100k_items():
    slide_id = create_slide_in_case("Aperio/CMU-1.svs")
    job_token = create_portal_app_and_running_job(
        io_spec={
            "my_slide": {"type": "wsi"},
            "my_collection": {"type": "collection", "items": {"type": "integer"}},
            "our_collection": {"type": "collection", "items": {"type": "point", "reference": "io.my_slide"}},
        },
        modes_spec={
            "standalone": {
                "inputs": ["my_slide"],
                "outputs": ["my_collection", "our_collection"],
            }
        },
        inputs={
            "my_slide": {
                "type": "wsi",
                "slide_id": slide_id,
            },
        },
    )
    job_id = job_token["job_id"]
    access_token = job_token["access_token"]
    headers = {"Authorization": f"Bearer {access_token}"}

    # post initial collection and retrieve id
    collection = {
        "name": "My Collection",
        "item_type": "integer",
        "creator_id": job_id,
        "creator_type": "job",
        "type": "collection",
        "items": [
            {
                "name": "My -1th Integer",
                "type": "integer",
                "value": -1,
                "creator_id": job_id,
                "creator_type": "job",
            }
        ],
    }

    url = f"{as_url}/v3/{job_id}/outputs/my_collection"
    response = requests.post(url, headers=headers, json=collection, timeout=5)
    assert response.status_code == 200
    my_collection = response.json()
    my_collection_id = my_collection["id"]

    # post initial collection and retrieve id
    collection = {
        "name": "Our Collection",
        "item_type": "point",
        "creator_id": job_id,
        "creator_type": "job",
        "type": "collection",
        "items": [
            {
                "name": "My Annotation",
                "type": "point",
                "creator_id": job_id,
                "creator_type": "job",
                "coordinates": [randint(0, 100000), randint(0, 100000)],
                "reference_id": slide_id,
                "reference_type": "wsi",
                "npp_created": 499,
            }
        ],
    }

    url = f"{as_url}/v3/{job_id}/outputs/our_collection"
    response = requests.post(url, headers=headers, json=collection, timeout=5)
    assert response.status_code == 200
    our_collection = response.json()
    our_collection_id = our_collection["id"]

    for j in range(0, 5):
        # extend collection for more items
        extension = {"items": []}
        for i in range(0, 10000):
            extension["items"].append(
                {
                    "name": f"My {i}th Integer",
                    "type": "integer",
                    "value": j * 100000 + i,
                    "creator_id": job_id,
                    "creator_type": "job",
                }
            )
        url = f"{as_url}/v3/{job_id}/collections/{my_collection_id}/items"
        response = requests.post(url, headers=headers, json=extension, timeout=500)
        assert response.status_code == 201

        # extend collection for more items
        extension = {"items": []}
        for i in range(0, 10000):
            extension["items"].append(
                {
                    "name": "My Annotation",
                    "type": "point",
                    "creator_id": job_id,
                    "creator_type": "job",
                    "coordinates": [randint(0, 100000), randint(0, 100000)],
                    "reference_id": slide_id,
                    "reference_type": "wsi",
                    "npp_created": 499,
                }
            )
        url = f"{as_url}/v3/{job_id}/collections/{our_collection_id}/items"
        response = requests.post(url, headers=headers, json=extension, timeout=500)
        assert response.status_code == 201
