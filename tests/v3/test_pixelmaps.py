import numpy as np
import requests

from ..singletons import as_url
from .helper import create_portal_app_and_running_job, create_slide_in_case


def test_pixelmaps():
    slide_id = create_slide_in_case("Aperio/CMU-1.svs")
    job_token = create_portal_app_and_running_job(
        io_spec={
            "my_slide": {"type": "wsi"},
            "my_pixelmap": {
                "type": "nominal_pixelmap",
                "reference": "io.my_slide",
                "element_classes": [
                    "org.empaia.my_vendor.my_app.v3.0.classes.zero",
                    "org.empaia.my_vendor.my_app.v3.0.classes.one",
                ],
            },
        },
        modes_spec={
            "standalone": {
                "inputs": ["my_slide"],
                "outputs": ["my_pixelmap"],
            },
        },
        inputs={},
    )
    job_id = job_token["job_id"]
    access_token = job_token["access_token"]
    headers = {"Authorization": f"Bearer {access_token}"}

    url = f"{as_url}/v3/{job_id}/outputs/my_pixelmap"

    pixelmap_level = {
        "slide_level": 1,
        "position_min_x": 0,
        "position_max_x": 2,
        "position_min_y": 0,
        "position_max_y": 2,
    }

    element_class_mapping = [
        {"number_value": 0, "class_value": "org.empaia.my_vendor.my_app.v3.0.classes.zero"},
        {"number_value": 1, "class_value": "org.empaia.my_vendor.my_app.v3.0.classes.one"},
    ]

    pixelmap = {
        "type": "nominal_pixelmap",
        "name": "NOMINAL PIXELMAP",
        "tilesize": 256,
        "element_type": "int8",
        "channel_count": 1,
        "levels": [pixelmap_level],
        "element_class_mapping": element_class_mapping,
        "reference_id": slide_id,
        "reference_type": "wsi",
        "creator_id": job_id,
        "creator_type": "job",
        "description": None,
        "neutral_value": 0,
    }

    r = requests.post(url, headers=headers, json=pixelmap, timeout=5)
    assert r.status_code == 200
    pixelmap = r.json()

    pixelmap_id = pixelmap["id"]
    level = 1
    tile_x = 0
    tile_y = 0

    tile_data = np.zeros((pixelmap["tilesize"], pixelmap["tilesize"]), dtype=pixelmap["element_type"])
    tile_data[42, 42] = 10

    url = f"{as_url}/v3/{job_id}/pixelmaps/{pixelmap_id}/level/{level}/position/{tile_x}/{tile_y}/data"

    headers["Accept-Encoding"] = None
    r = requests.put(url, headers=headers, data=tile_data.tobytes(), timeout=5)
    assert r.status_code == 204

    headers["Accept-Encoding"] = None
    r = requests.get(url, headers=headers, timeout=5)
    assert r.status_code == 200
    assert r.content == tile_data.tobytes()
    content_encoding = r.headers.get("Content-Encoding")
    assert content_encoding is None

    headers["Accept-Encoding"] = "gzip"
    r = requests.get(url, headers=headers, timeout=5)
    assert r.status_code == 200
    assert r.content == tile_data.tobytes()
    content_encoding = r.headers.get("Content-Encoding")
    assert content_encoding is not None
    assert "gzip" in content_encoding

    headers["Accept-Encoding"] = "gzip"
    r = requests.put(url, headers=headers, data=tile_data.tobytes(), timeout=5)
    assert r.status_code == 204

    headers["Accept-Encoding"] = None
    r = requests.get(url, headers=headers, timeout=5)
    assert r.status_code == 200
    assert r.content == tile_data.tobytes()
    content_encoding = r.headers.get("Content-Encoding")
    assert content_encoding is None

    tile_data_1_1 = np.zeros((pixelmap["tilesize"], pixelmap["tilesize"]), dtype=pixelmap["element_type"])
    tile_data_1_1[1, 1] = -10
    tile_data_2_1 = np.zeros((pixelmap["tilesize"], pixelmap["tilesize"]), dtype=pixelmap["element_type"])
    tile_data_2_1[2, 1] = -5
    tile_data_1_2 = np.zeros((pixelmap["tilesize"], pixelmap["tilesize"]), dtype=pixelmap["element_type"])
    tile_data_1_2[1, 2] = 5
    tile_data_2_2 = np.zeros((pixelmap["tilesize"], pixelmap["tilesize"]), dtype=pixelmap["element_type"])
    tile_data_2_2[2, 2] = 10

    tiles = [tile_data_1_1, tile_data_2_1, tile_data_1_2, tile_data_2_2]
    byte_data = b"".join([tile.tobytes() for tile in tiles])

    level = 1
    start_x = 1
    start_y = 1
    end_x = 2
    end_y = 2

    url = (
        f"{as_url}/v3/{job_id}/pixelmaps/{pixelmap_id}/level/{level}"
        f"/position/start/{start_x}/{start_y}/end/{end_x}/{end_y}/data"
    )

    headers["Accept-Encoding"] = None
    r = requests.put(url, headers=headers, data=byte_data, timeout=5)
    assert r.status_code == 204

    headers["Accept-Encoding"] = None
    r = requests.get(url, headers=headers, timeout=5)
    assert r.status_code == 200
    assert r.content == byte_data
    content_encoding = r.headers.get("Content-Encoding")
    assert content_encoding is None

    headers["Accept-Encoding"] = "gzip"
    r = requests.get(url, headers=headers, timeout=5)
    assert r.status_code == 200
    assert r.content == byte_data
    content_encoding = r.headers.get("Content-Encoding")
    assert content_encoding is not None
    assert "gzip" in content_encoding

    headers["Accept-Encoding"] = "gzip"
    r = requests.put(url, headers=headers, data=byte_data, timeout=5)
    assert r.status_code == 204

    headers["Accept-Encoding"] = None
    r = requests.get(url, headers=headers, timeout=5)
    assert r.status_code == 200
    assert r.content == byte_data
    content_encoding = r.headers.get("Content-Encoding")
    assert content_encoding is None

    url = f"{as_url}/v3/{job_id}/pixelmaps/{pixelmap_id}/info"
    r = requests.get(url, headers=headers, timeout=5)
    assert r.status_code == 200
    info = r.json()
    for key in ["extent", "num_levels", "pixel_size_nm", "levels"]:
        assert key in info
