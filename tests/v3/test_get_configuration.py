import requests

from ..singletons import as_url
from .helper import (
    add_customer_app_config,
    add_global_app_config,
    create_portal_app,
    create_portal_app_and_running_job,
    create_running_job,
)


def test_no_configuration():
    job_token = create_portal_app_and_running_job(
        io_spec={},
        modes_spec={
            "standalone": {
                "inputs": [],
                "outputs": [],
            }
        },
        inputs={},
    )
    job_id = job_token["job_id"]
    access_token = job_token["access_token"]
    headers = {"Authorization": f"Bearer {access_token}"}

    url = f"{as_url}/v3/{job_id}/configuration"
    response = requests.get(url, headers=headers, timeout=5)
    assert response.status_code == 200
    config = response.json()
    assert config == {
        "global": {},
        "customer": {},
    }


def test_global_and_customer_configuration():
    app_id = create_portal_app(
        io_spec={},
        modes_spec={
            "standalone": {
                "inputs": [],
                "outputs": [],
            }
        },
        permissions_spec={},
        config_spec={
            "global": {
                "api_key": {"type": "string", "optional": False},
            },
            "customer": {
                "special_key": {"type": "string", "optional": False},
            },
        },
    )

    global_config = {"api_key": "top secret"}
    customer_config = {"special_key": "something special"}
    add_global_app_config(app_id, global_config)
    add_customer_app_config(app_id, customer_config, "dummy-orga-id")

    job_token = create_running_job(app_id, mode="STANDALONE", inputs={})
    job_id = job_token["job_id"]
    access_token = job_token["access_token"]
    headers = {"Authorization": f"Bearer {access_token}"}

    url = f"{as_url}/v3/{job_id}/configuration"
    response = requests.get(url, headers=headers, timeout=5)
    assert response.status_code == 200
    config = response.json()
    assert config == {
        "global": global_config,
        "customer": customer_config,
    }


def test_get_global_only_configuration():
    app_id = create_portal_app(
        io_spec={},
        modes_spec={
            "standalone": {
                "inputs": [],
                "outputs": [],
            }
        },
        permissions_spec={},
        config_spec={
            "global": {
                "api_key": {"type": "string", "optional": False},
            },
        },
    )

    global_config = {"api_key": "top secret"}
    add_global_app_config(app_id, global_config)

    job_token = create_running_job(app_id, mode="STANDALONE", inputs={})
    job_id = job_token["job_id"]
    access_token = job_token["access_token"]
    headers = {"Authorization": f"Bearer {access_token}"}

    url = f"{as_url}/v3/{job_id}/configuration"
    response = requests.get(url, headers=headers, timeout=5)
    assert response.status_code == 200
    config = response.json()
    assert config == {
        "global": global_config,
        "customer": {},
    }


def test_get_customer_only_configuration():
    app_id = create_portal_app(
        io_spec={},
        modes_spec={
            "standalone": {
                "inputs": [],
                "outputs": [],
            }
        },
        permissions_spec={},
        config_spec={
            "customer": {
                "api_key": {"type": "string", "optional": False},
            },
        },
    )

    customer_config = {"api_key": "top secret"}
    add_customer_app_config(app_id, customer_config, "dummy-orga-id")

    job_token = create_running_job(app_id, mode="STANDALONE", inputs={})
    job_id = job_token["job_id"]
    access_token = job_token["access_token"]
    headers = {"Authorization": f"Bearer {access_token}"}

    url = f"{as_url}/v3/{job_id}/configuration"
    response = requests.get(url, headers=headers, timeout=5)
    assert response.status_code == 200
    config = response.json()
    assert config == {
        "global": {},
        "customer": customer_config,
    }
