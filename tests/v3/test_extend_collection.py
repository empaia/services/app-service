import uuid

import requests

from ..singletons import as_url, mds_url
from .helper import create_portal_app_and_running_job


def test_extend_collection():
    job_token = create_portal_app_and_running_job(
        io_spec={
            "my_collection": {"type": "collection", "items": {"type": "integer"}},
            "their_collection": {"type": "collection", "items": {"type": "integer"}},
        },
        modes_spec={
            "standalone": {
                "inputs": ["their_collection"],
                "outputs": ["my_collection"],
            }
        },
        inputs={
            "their_collection": {
                "name": "Their Collection",
                "type": "collection",
                "item_type": "integer",
                "items": [],
            },
        },
    )
    job_id = job_token["job_id"]
    access_token = job_token["access_token"]
    headers = {"Authorization": f"Bearer {access_token}"}

    # post initial collection and retrieve id
    collection = {
        "name": "My Collection",
        "item_type": "integer",
        "creator_id": job_id,
        "creator_type": "job",
        "type": "collection",
        "items": [
            {"name": "My First Integer", "type": "integer", "value": 42, "creator_id": job_id, "creator_type": "job"}
        ],
    }

    url = f"{as_url}/v3/{job_id}/outputs/my_collection"
    response = requests.post(url, headers=headers, json=collection, timeout=5)
    assert response.status_code == 200
    collection = response.json()
    collection_id = collection["id"]

    # extend collection for more items
    extension = {
        "items": [
            {"name": "My Second Integer", "type": "integer", "value": 43, "creator_id": job_id, "creator_type": "job"},
            {"name": "My Third Integer", "type": "integer", "value": 44, "creator_id": job_id, "creator_type": "job"},
        ]
    }
    url = f"{as_url}/v3/{job_id}/collections/{collection_id}/items"
    response = requests.post(url, headers=headers, json=extension, timeout=5)
    assert response.status_code == 201
    item_list_response = response.json()
    assert item_list_response["item_count"] == 2
    assert item_list_response["items"][0]["name"] == "My Second Integer"
    assert item_list_response["items"][0]["type"] == "integer"
    assert item_list_response["items"][0]["value"] == 43
    assert item_list_response["items"][0]["creator_id"] == job_id
    assert item_list_response["items"][0]["creator_type"] == "job"
    assert item_list_response["items"][1]["name"] == "My Third Integer"
    assert item_list_response["items"][1]["type"] == "integer"
    assert item_list_response["items"][1]["value"] == 44
    assert item_list_response["items"][1]["creator_id"] == job_id
    assert item_list_response["items"][1]["creator_type"] == "job"

    # check whether collection was extended internally in MDS
    url = f"{mds_url}/v3/collections/{collection_id}"
    response = requests.get(url, timeout=5)
    assert response.status_code == 200
    collection = response.json()
    assert collection["item_count"] == 3
    # ## NOTE: The following was never guaranteed (order of items according to date posted)
    # assert collection["items"][0]["name"] == "My First Integer"
    # assert collection["items"][0]["type"] == "integer"
    # assert collection["items"][0]["value"] == 42
    # assert collection["items"][0]["creator_id"] == job_id
    # assert collection["items"][0]["creator_type"] == "job"
    # assert collection["items"][1]["name"] == "My Second Integer"
    # assert collection["items"][1]["type"] == "integer"
    # assert collection["items"][1]["value"] == 43
    # assert collection["items"][1]["creator_id"] == job_id
    # assert collection["items"][1]["creator_type"] == "job"
    # assert collection["items"][2]["name"] == "My Third Integer"
    # assert collection["items"][2]["type"] == "integer"
    # assert collection["items"][2]["value"] == 44
    # assert collection["items"][2]["creator_id"] == job_id
    # assert collection["items"][2]["creator_type"] == "job"

    # invalid collection id
    invalid_collection_id = str(uuid.uuid4())
    url = f"{as_url}/v3/{job_id}/collections/{invalid_collection_id}/query"
    query = {}
    response = requests.put(url, headers=headers, json=query, timeout=5)
    print(response.text)
    assert response.status_code == 404
