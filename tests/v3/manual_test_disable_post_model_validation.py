from random import randint

import requests

from ..singletons import as_url
from .helper import create_portal_app_and_running_job, create_slide_in_case

# ####################################################################################
# (1) enable / disable app-service post model validation via env or docker-compose
#     COMPOSE_AS_ENABLE_POST_ITEM_LIST_MODEL_VALIDATION=False
#     COMPOSE_AS_ENABLE_POST_ITEM_LIST_MODEL_VALIDATION=False
#     docker-compose up -d --build
#
# (1.5) Opt: already start logs
#     docker-compose logs -f annotation-service medical-data-service app-service
# (2) run test
#     pytest tests/v3/manual_test_disable_post_model_validation.py
#
# (3) check app-service / medical-data-service / annotation-service logs
# to see where the model validation fails
#     docker-compose logs app-service
#     docker-compose logs medical-data-service
#     docker-compose logs annotation-service
#
# TODO: make automated test by checking logs via code
# ####################################################################################


def test_disable_post_model_validation():
    slide_id = create_slide_in_case("Aperio/CMU-1.svs")
    job_token = create_portal_app_and_running_job(
        io_spec={
            "my_slide": {"type": "wsi"},
            "my_collection": {"type": "collection", "items": {"type": "integer"}},
            "our_collection": {"type": "collection", "items": {"type": "point", "reference": "io.my_slide"}},
        },
        modes_spec={
            "standalone": {
                "inputs": ["my_slide"],
                "outputs": ["my_collection", "our_collection"],
            }
        },
        inputs={
            "my_slide": {
                "type": "wsi",
                "slide_id": slide_id,
            },
        },
    )
    job_id = job_token["job_id"]
    access_token = job_token["access_token"]
    headers = {"Authorization": f"Bearer {access_token}"}

    # post initial collection and retrieve id
    collection = {
        "name": "My Collection",
        "item_type": "integer",
        "creator_id": job_id,
        "creator_type": "job",
        "type": "collection",
        "items": [
            {
                "name": "My -1th Integer",
                "typeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee": "integer",  # invalid model
                "value": -1,
                "creator_id": job_id,
                "creator_type": "job",
            }
        ],
    }

    url = f"{as_url}/v3/{job_id}/outputs/my_collection"
    response = requests.post(url, headers=headers, json=collection, timeout=5)
    assert response.status_code == 422

    # post initial collection and retrieve id
    collection = {
        "name": "Our Collection",
        "item_type": "point",
        "creator_id": job_id,
        "creator_type": "job",
        "type": "collection",
        "items": [
            {
                "name": "My Annotation",
                "type": "point",
                "creator_id": job_id,
                "creator_type": "jobbbbbbbbbbbbbbbbbbbbbbbbbbbbbb",  # invalid value
                "coordinates": [randint(0, 100000), randint(0, 100000)],
                "reference_id": slide_id,
                "reference_type": "wsi",
                "npp_created": 499,
            }
        ],
    }

    url = f"{as_url}/v3/{job_id}/outputs/our_collection"
    response = requests.post(url, headers=headers, json=collection, timeout=5)
    assert response.status_code == 422
