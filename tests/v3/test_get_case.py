import requests

from ..singletons import as_url
from .helper import (
    add_job_to_open_examination,
    create_case,
    create_portal_app_and_running_job,
)


def test_get_case():
    job_token = create_portal_app_and_running_job(
        io_spec={},
        modes_spec={
            "standalone": {
                "inputs": [],
                "outputs": [],
            },
        },
        inputs={},
        mode="STANDALONE",
    )
    job_id = job_token["job_id"]
    case_id = create_case()
    add_job_to_open_examination(job_id, case_id)

    access_token = job_token["access_token"]
    headers = {"Authorization": f"Bearer {access_token}"}

    url = f"{as_url}/v3/{job_id}/case"
    response = requests.get(url, headers=headers, timeout=5)
    assert response.status_code == 200
    case_ = response.json()
    assert case_ == {"id": case_id}
