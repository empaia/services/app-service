import requests

from ..singletons import as_url
from .helper import create_portal_app_and_running_job, create_slide_in_case


def test_get_mode_standalone():
    job_token = create_portal_app_and_running_job(
        io_spec={},
        modes_spec={
            "standalone": {
                "inputs": [],
                "outputs": [],
            },
        },
        inputs={},
        mode="STANDALONE",
    )
    job_id = job_token["job_id"]
    access_token = job_token["access_token"]
    headers = {"Authorization": f"Bearer {access_token}"}

    url = f"{as_url}/v3/{job_id}/mode"
    response = requests.get(url, headers=headers, timeout=5)
    assert response.status_code == 200
    assert response.json() == {"mode": "STANDALONE"}


def test_get_mode_preprocessing():
    slide_id = create_slide_in_case("Aperio/CMU-1.svs")
    job_token = create_portal_app_and_running_job(
        io_spec={"my_slide": {"type": "wsi"}},
        modes_spec={
            "preprocessing": {
                "inputs": ["my_slide"],
                "outputs": [],
            },
        },
        inputs={
            "my_slide": {
                "type": "wsi",
                "slide_id": slide_id,
            },
        },
        mode="PREPROCESSING",
    )
    job_id = job_token["job_id"]
    access_token = job_token["access_token"]
    headers = {"Authorization": f"Bearer {access_token}"}

    url = f"{as_url}/v3/{job_id}/mode"
    response = requests.get(url, headers=headers, timeout=5)
    assert response.status_code == 200
    assert response.json() == {"mode": "PREPROCESSING"}


def test_get_mode_postprocessing():
    slide_id = create_slide_in_case("Aperio/CMU-1.svs")
    job_token = create_portal_app_and_running_job(
        io_spec={"my_slide": {"type": "wsi"}},
        modes_spec={
            "preprocessing": {
                "inputs": ["my_slide"],
                "outputs": [],
            },
            "postprocessing": {"inputs": [], "outputs": [], "containerized": False},
        },
        inputs={
            "my_slide": {
                "type": "wsi",
                "slide_id": slide_id,
            },
        },
        mode="POSTPROCESSING",
    )
    job_id = job_token["job_id"]
    access_token = job_token["access_token"]
    headers = {"Authorization": f"Bearer {access_token}"}

    url = f"{as_url}/v3/{job_id}/mode"
    response = requests.get(url, headers=headers, timeout=5)
    assert response.status_code == 200
    assert response.json() == {"mode": "POSTPROCESSING"}
