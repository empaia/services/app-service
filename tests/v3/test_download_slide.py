import requests

from ..singletons import as_url, mds_url
from .helper import create_portal_app_and_running_job, create_slide_in_case


def test_download_slide():
    slide_id = create_slide_in_case("Aperio/CMU-1.svs")
    job_token = create_portal_app_and_running_job(
        io_spec={"my_slide": {"type": "wsi"}},
        modes_spec={
            "standalone": {
                "inputs": ["my_slide"],
                "outputs": [],
            }
        },
        permissions_spec={"wsi_raw_file_access": True},
        inputs={
            "my_slide": {
                "type": "wsi",
                "slide_id": slide_id,
            }
        },
    )
    print(job_token)
    job_id = job_token["job_id"]
    access_token = job_token["access_token"]
    headers = {"Authorization": f"Bearer {access_token}"}

    url = f"{as_url}/v3/{job_id}/slides/{slide_id}/download"
    response = requests.get(url, headers=headers, timeout=10)
    print(response.text)
    assert response.status_code == 200

    ref_url = f"{mds_url}/v3/slides/{slide_id}/download"
    ref_response = requests.get(ref_url, timeout=10)
    assert ref_response.status_code == 200
    assert response.content == ref_response.content

    # invalid slide id
    url = f"{as_url}/v3/{job_id}/slides/some-invalid-id/download"
    response = requests.get(url, headers=headers, timeout=5)
    assert response.status_code == 404


def test_download_slide_without_permission():
    slide_id = create_slide_in_case("Aperio/CMU-1.svs")
    job_token = create_portal_app_and_running_job(
        io_spec={"my_slide": {"type": "wsi"}},
        modes_spec={
            "standalone": {
                "inputs": ["my_slide"],
                "outputs": [],
            }
        },
        inputs={
            "my_slide": {
                "type": "wsi",
                "slide_id": slide_id,
            }
        },
    )
    job_id = job_token["job_id"]
    access_token = job_token["access_token"]
    headers = {"Authorization": f"Bearer {access_token}"}

    url = f"{as_url}/v3/{job_id}/slides/{slide_id}/download"
    response = requests.get(url, headers=headers, timeout=10)
    assert response.status_code == 403
