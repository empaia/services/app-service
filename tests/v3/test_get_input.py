import requests

from ..singletons import as_url
from .helper import (
    create_portal_app_and_running_job,
    create_slide_in_case,
    update_slide,
)


def test_get_input():
    slide_id = create_slide_in_case("Aperio/CMU-1.svs")
    job_token = create_portal_app_and_running_job(
        io_spec={
            "my_slide": {"type": "wsi"},
            "my_primitive": {"type": "integer"},
            "my_annotation": {"type": "point", "reference": "io.my_slide"},
            "my_collection": {"type": "collection", "items": {"type": "integer"}},
            "my_prediction": {"type": "integer"},
        },
        modes_spec={
            "standalone": {
                "inputs": [
                    "my_slide",
                    "my_primitive",
                    "my_annotation",
                    "my_collection",
                ],
                "outputs": ["my_prediction"],
            },
        },
        inputs={
            "my_slide": {
                "type": "wsi",
                "slide_id": slide_id,
            },
            "my_primitive": {
                "name": "My Primitive",
                "type": "integer",
                "value": 42,
            },
            "my_annotation": {
                "name": "My Annotation",
                "type": "point",
                "reference_id": slide_id,
                "reference_type": "wsi",
                "coordinates": [1024, 2048],
                "npp_created": 499,
            },
            "my_collection": {
                "name": "My Collection",
                "type": "collection",
                "item_type": "integer",
                "items": [
                    {
                        "name": "My First Integer",
                        "type": "integer",
                        "value": 42,
                    },
                    {
                        "name": "My Second Integer",
                        "type": "integer",
                        "value": 43,
                    },
                ],
            },
        },
    )
    job_id = job_token["job_id"]
    access_token = job_token["access_token"]
    headers = {"Authorization": f"Bearer {access_token}"}

    # slide
    url = f"{as_url}/v3/{job_id}/inputs/my_slide"
    response = requests.get(url, headers=headers, timeout=5)
    assert response.status_code == 200
    slide = response.json()
    assert slide["channel_depth"] == 8
    assert slide["num_levels"] == 3
    assert slide["extent"] == {"x": 46000, "y": 32914, "z": 1}
    assert "format" in slide
    assert slide["raw_download"]
    assert not slide["tissue"]
    assert not slide["stain"]

    # clinical slide properties can be utilized by apps
    update_slide(slide_id, tissue="BREAST", stain="H_AND_E")
    url = f"{as_url}/v3/{job_id}/inputs/my_slide"
    response = requests.get(url, headers=headers, timeout=5)
    assert response.status_code == 200
    slide = response.json()
    assert slide["tissue"] == "BREAST"
    assert slide["stain"] == "H_AND_E"

    # primitive
    url = f"{as_url}/v3/{job_id}/inputs/my_primitive"
    response = requests.get(url, headers=headers, timeout=5)
    assert response.status_code == 200
    primitive = response.json()
    assert primitive["name"] == "My Primitive"
    assert primitive["type"] == "integer"
    assert primitive["value"] == 42

    # annotation
    url = f"{as_url}/v3/{job_id}/inputs/my_annotation"
    response = requests.get(url, headers=headers, timeout=5)
    assert response.status_code == 200
    annotation = response.json()
    assert annotation["name"] == "My Annotation"
    assert annotation["type"] == "point"
    assert annotation["coordinates"] == [1024, 2048]

    # collection
    url = f"{as_url}/v3/{job_id}/inputs/my_collection"
    response = requests.get(url, headers=headers, timeout=5)
    assert response.status_code == 200
    collection = response.json()
    assert collection["name"] == "My Collection"
    assert collection["type"] == "collection"
    assert collection["item_type"] == "integer"
    assert collection["item_count"] == 2
    assert collection["items"][0]["name"] in ["My First Integer", "My Second Integer"]
    assert collection["items"][0]["type"] == "integer"
    assert collection["items"][0]["value"] in [42, 43]
    assert collection["items"][1]["name"] in ["My First Integer", "My Second Integer"]
    assert collection["items"][1]["type"] == "integer"
    assert collection["items"][1]["value"] in [42, 43]

    # invalid input
    url = f"{as_url}/v3/{job_id}/inputs/invalid_input"
    response = requests.get(url, headers=headers, timeout=5)
    assert response.status_code == 404
