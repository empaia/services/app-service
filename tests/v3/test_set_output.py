import requests

from ..singletons import as_url
from .helper import (
    create_portal_app_and_running_job,
    create_questionnaire,
    create_slide_in_case,
)


def test_set_output():
    slide_id = create_slide_in_case("Aperio/CMU-1.svs")
    job_token = create_portal_app_and_running_job(
        io_spec={
            "my_slide": {"type": "wsi"},
            "my_primitive": {"type": "integer"},
            "my_annotation": {"type": "point", "reference": "io.my_slide"},
            "my_collection": {"type": "collection", "items": {"type": "integer"}},
        },
        modes_spec={
            "standalone": {
                "inputs": ["my_slide"],
                "outputs": ["my_primitive", "my_annotation", "my_collection"],
            },
        },
        inputs={
            "my_slide": {
                "type": "wsi",
                "slide_id": slide_id,
            },
        },
    )
    job_id = job_token["job_id"]
    access_token = job_token["access_token"]
    headers = {"Authorization": f"Bearer {access_token}"}

    # primitive
    url = f"{as_url}/v3/{job_id}/outputs/my_primitive"
    payload = {
        "name": "My Primitive",
        "type": "integer",
        "creator_id": job_id,
        "creator_type": "job",
        "value": 42,
    }
    response = requests.post(url, headers=headers, json=payload, timeout=5)
    assert response.status_code == 200
    primitive = response.json()
    assert "id" in primitive
    assert primitive["name"] == "My Primitive"
    assert primitive["type"] == "integer"
    assert primitive["value"] == 42
    assert primitive["creator_id"] == job_id
    assert primitive["creator_type"] == "job"

    # annotation
    url = f"{as_url}/v3/{job_id}/outputs/my_annotation"
    payload = {
        "name": "My Annotation",
        "type": "point",
        "creator_id": job_id,
        "creator_type": "job",
        "coordinates": [1024, 2048],
        "reference_id": slide_id,
        "reference_type": "wsi",
        "npp_created": 499,
    }
    response = requests.post(url, headers=headers, json=payload, timeout=5)
    assert response.status_code == 200
    annotation = response.json()
    assert "id" in annotation
    assert annotation["name"] == "My Annotation"
    assert annotation["type"] == "point"
    assert annotation["coordinates"] == [1024, 2048]
    assert annotation["creator_id"] == job_id
    assert annotation["creator_type"] == "job"
    assert annotation["reference_id"] == slide_id
    assert annotation["reference_type"] == "wsi"
    assert annotation["npp_created"] == 499

    # collection
    url = f"{as_url}/v3/{job_id}/outputs/my_collection"
    payload = {
        "name": "My Collection",
        "type": "collection",
        "creator_id": job_id,
        "creator_type": "job",
        "reference_id": slide_id,
        "reference_type": "wsi",
        "item_type": "integer",
        "items": [
            {
                "name": "My First Integer",
                "type": "integer",
                "creator_id": job_id,
                "creator_type": "job",
                "value": 42,
            },
            {
                "name": "My Second Integer",
                "type": "integer",
                "creator_id": job_id,
                "creator_type": "job",
                "value": 43,
            },
        ],
    }
    response = requests.post(url, headers=headers, json=payload, timeout=5)
    assert response.status_code == 200
    collection = response.json()
    assert "id" in collection
    assert collection["name"] == "My Collection"
    assert collection["type"] == "collection"
    assert collection["creator_id"] == job_id
    assert collection["creator_type"] == "job"
    assert "id" in collection["items"][0]
    assert collection["items"][0]["name"] == "My First Integer"
    assert collection["items"][0]["type"] == "integer"
    assert collection["items"][0]["creator_id"] == job_id
    assert collection["items"][0]["creator_type"] == "job"
    assert collection["items"][0]["value"] == 42
    assert "id" in collection["items"][1]
    assert collection["items"][1]["name"] == "My Second Integer"
    assert collection["items"][1]["type"] == "integer"
    assert collection["items"][1]["creator_id"] == job_id
    assert collection["items"][1]["creator_type"] == "job"
    assert collection["items"][1]["value"] == 43

    # invalid output
    url = f"{as_url}/v3/{job_id}/outputs/invalid_output"
    response = requests.post(url, headers=headers, json=payload, timeout=5)
    assert response.status_code == 404


def test_set_output_pixelmap():
    slide_id = create_slide_in_case("Aperio/CMU-1.svs")
    job_token = create_portal_app_and_running_job(
        io_spec={
            "my_slide": {"type": "wsi"},
            "my_pixelmap": {
                "type": "nominal_pixelmap",
                "reference": "io.my_slide",
            },
        },
        modes_spec={
            "preprocessing": {
                "inputs": ["my_slide"],
                "outputs": ["my_pixelmap"],
            },
        },
        inputs={
            "my_slide": {
                "type": "wsi",
                "slide_id": slide_id,
            },
        },
        mode="PREPROCESSING",
    )
    job_id = job_token["job_id"]
    access_token = job_token["access_token"]
    headers = {"Authorization": f"Bearer {access_token}"}

    # pixelmap
    url = f"{as_url}/v3/{job_id}/outputs/my_pixelmap"
    payload = {
        "name": "My Pixelmap",
        "type": "nominal_pixelmap",
        "creator_id": job_id,
        "creator_type": "job",
        "reference_id": slide_id,
        "reference_type": "wsi",
        "element_type": "uint8",
        "levels": [{"slide_level": 0}],
        "channel_count": 1,
        "tilesize": 256,
        "element_class_mapping": [
            {"number_value": 0, "class_value": "org.empaia.vendor_name.tutorial_app_13.v3.0.classes.tissue"}
        ],
        "neutral_value": 0,
    }
    response = requests.post(url, headers=headers, json=payload, timeout=5)
    print(response.content)
    assert response.status_code == 200
    pixelmap = response.json()
    assert "id" in pixelmap
    assert pixelmap["name"] == "My Pixelmap"
    print(pixelmap["id"])


def test_post_and_put_output_fhir_questionnaire_response():
    slide_id = create_slide_in_case("Aperio/CMU-1.svs")
    questionnaire_resource = create_questionnaire()
    output_key = "my_questionnaire_response"
    job_token = create_portal_app_and_running_job(
        io_spec={
            "my_slide": {"type": "wsi"},
            "my_questionnaire": {
                "type": "fhir_questionnaire",
                "selectors": ["org.empaia.my_vendor.my_app.v3.1.selectors.something.something-else"],
            },
            output_key: {"type": "fhir_questionnaire_response"},
        },
        modes_spec={
            "preprocessing": {
                "inputs": ["my_slide", "my_questionnaire"],
                "outputs": [output_key],
            },
        },
        inputs={
            "my_slide": {
                "type": "wsi",
                "slide_id": slide_id,
            },
            "my_questionnaire": {
                "type": "fhir_questionnaire",
                "fhir_questionnaire_id": (
                    f'{questionnaire_resource["id"]}||{questionnaire_resource["meta"]["versionId"]}'
                ),
            },
        },
        mode="PREPROCESSING",
    )
    job_id = job_token["job_id"]
    access_token = job_token["access_token"]
    headers = {"Authorization": f"Bearer {access_token}"}

    # POST fhir_questionnaire_response
    url = f"{as_url}/v3/{job_id}/outputs/{output_key}"
    post_questionnaire_response_resource = {
        "resourceType": "QuestionnaireResponse",
        "questionnaire": f'Questionnaire/{questionnaire_resource["id"]}',
        "status": "in-progress",
    }
    res = requests.post(url, headers=headers, json=post_questionnaire_response_resource, timeout=5)
    print(res.content)
    assert res.status_code == 200
    post_questionnaire_response = res.json()
    assert post_questionnaire_response["resourceType"] == "QuestionnaireResponse"
    assert post_questionnaire_response["questionnaire"] == f"Questionnaire/{questionnaire_resource['id']}"
    assert post_questionnaire_response["status"] == "in-progress"
    assert int(post_questionnaire_response["meta"]["versionId"]) != 1
    print(post_questionnaire_response["id"])

    # PUT fhir_questionnaire_response
    url = f"{as_url}/v3/{job_id}/fhir/questionnaire-responses/{post_questionnaire_response['id']}"
    put_questionnaire_response_resource = {
        "resourceType": "QuestionnaireResponse",
        "questionnaire": f'Questionnaire/{questionnaire_resource["id"]}',
        "status": "completed",
    }
    res = requests.put(url, headers=headers, json=put_questionnaire_response_resource, timeout=5)
    print(res.content)
    assert res.status_code == 200
    put_questionnaire_response = res.json()
    assert put_questionnaire_response["resourceType"] == "QuestionnaireResponse"
    assert put_questionnaire_response["questionnaire"] == f'Questionnaire/{questionnaire_resource["id"]}'
    assert put_questionnaire_response["status"] == "completed"
    assert (
        int(put_questionnaire_response["meta"]["versionId"])
        == int(post_questionnaire_response["meta"]["versionId"]) + 1
    )
    print(put_questionnaire_response["id"])


def test_post_output_fhir_questionnaire_response_and_wrong_put():
    slide_id = create_slide_in_case("Aperio/CMU-1.svs")
    questionnaire_resource = create_questionnaire()
    output_key = "my_questionnaire_response"
    job_token = create_portal_app_and_running_job(
        io_spec={
            "my_slide": {"type": "wsi"},
            "my_questionnaire": {
                "type": "fhir_questionnaire",
                "selectors": ["org.empaia.my_vendor.my_app.v3.1.selectors.something.something-else"],
            },
            output_key: {"type": "fhir_questionnaire_response"},
        },
        modes_spec={
            "preprocessing": {
                "inputs": ["my_slide", "my_questionnaire"],
                "outputs": [output_key],
            },
        },
        inputs={
            "my_slide": {
                "type": "wsi",
                "slide_id": slide_id,
            },
            "my_questionnaire": {
                "type": "fhir_questionnaire",
                "fhir_questionnaire_id": (
                    f'{questionnaire_resource["id"]}||{questionnaire_resource["meta"]["versionId"]}'
                ),
            },
        },
        mode="PREPROCESSING",
    )
    job_id = job_token["job_id"]
    access_token = job_token["access_token"]
    headers = {"Authorization": f"Bearer {access_token}"}

    # POST fhir_questionnaire_response
    url = f"{as_url}/v3/{job_id}/outputs/{output_key}"
    post_questionnaire_response_resource = {
        "resourceType": "QuestionnaireResponse",
        "questionnaire": f'Questionnaire/{questionnaire_resource["id"]}',
        "status": "in-progress",
    }
    res = requests.post(url, headers=headers, json=post_questionnaire_response_resource, timeout=5)
    print(res.content)
    assert res.status_code == 200
    post_questionnaire_response = res.json()
    assert post_questionnaire_response["resourceType"] == "QuestionnaireResponse"
    assert post_questionnaire_response["questionnaire"] == f"Questionnaire/{questionnaire_resource['id']}"
    assert post_questionnaire_response["status"] == "in-progress"
    assert int(post_questionnaire_response["meta"]["versionId"]) != 1
    print(post_questionnaire_response["id"])

    # PUT fhir_questionnaire_response with non-existing logical_id
    url = f"{as_url}/v3/{job_id}/fhir/questionnaire-responses/non_existing_logical_id"
    put_questionnaire_response_resource = {
        "resourceType": "QuestionnaireResponse",
        "questionnaire": f'Questionnaire/{questionnaire_resource["id"]}',
        "status": "completed",
    }
    res = requests.put(url, headers=headers, json=put_questionnaire_response_resource, timeout=5)
    print(res.content)
    assert res.status_code == 404


def test_only_put_output_fhir_questionnaire_response():
    slide_id = create_slide_in_case("Aperio/CMU-1.svs")
    questionnaire_resource = create_questionnaire()
    output_key = "my_questionnaire_response"
    job_token = create_portal_app_and_running_job(
        io_spec={
            "my_slide": {"type": "wsi"},
            "my_questionnaire": {
                "type": "fhir_questionnaire",
                "selectors": ["org.empaia.my_vendor.my_app.v3.1.selectors.something.something-else"],
            },
            output_key: {"type": "fhir_questionnaire_response"},
        },
        modes_spec={
            "preprocessing": {
                "inputs": ["my_slide", "my_questionnaire"],
                "outputs": [output_key],
            },
        },
        inputs={
            "my_slide": {
                "type": "wsi",
                "slide_id": slide_id,
            },
            "my_questionnaire": {
                "type": "fhir_questionnaire",
                "fhir_questionnaire_id": (
                    f'{questionnaire_resource["id"]}||{questionnaire_resource["meta"]["versionId"]}'
                ),
            },
        },
        mode="PREPROCESSING",
    )
    job_id = job_token["job_id"]
    access_token = job_token["access_token"]
    headers = {"Authorization": f"Bearer {access_token}"}

    # PUT fhir_questionnaire_response that was previously not posted
    url = f"{as_url}/v3/{job_id}/fhir/questionnaire-responses/non_existing_logical_id"
    put_questionnaire_response_resource = {
        "resourceType": "QuestionnaireResponse",
        "questionnaire": f'Questionnaire/{questionnaire_resource["id"]}',
        "status": "completed",
    }
    res = requests.put(url, headers=headers, json=put_questionnaire_response_resource, timeout=5)
    print(res.content)
    assert res.status_code == 404
