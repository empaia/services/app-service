import requests

from ..singletons import as_url
from .helper import create_running_job, create_slide_in_case


def test_set_output():
    slide_id = create_slide_in_case("Aperio/CMU-1.svs")
    job_token = create_running_job(
        inputs_spec={
            "my_slide": {"type": "wsi"},
        },
        outputs_spec={
            "my_primitive": {"type": "integer"},
            "my_annotation": {"type": "point", "reference": "inputs.my_slide"},
            "my_collection": {"type": "collection", "items": {"type": "integer"}},
        },
        inputs={
            "my_slide": {
                "type": "wsi",
                "slide_id": slide_id,
            },
        },
    )
    job_id = job_token["job_id"]
    access_token = job_token["access_token"]
    headers = {"Authorization": f"Bearer {access_token}"}

    # primitive
    url = f"{as_url}/v0/{job_id}/outputs/my_primitive"
    payload = {
        "name": "My Primitive",
        "type": "integer",
        "value": 42,
    }
    response = requests.post(url, headers=headers, json=payload, timeout=5)
    assert response.status_code == 200
    primitive = response.json()
    assert "id" in primitive
    assert primitive["name"] == "My Primitive"
    assert primitive["type"] == "integer"
    assert primitive["value"] == 42
    assert primitive["creator_id"] == job_id
    assert primitive["creator_type"] == "job"

    # annotation
    url = f"{as_url}/v0/{job_id}/outputs/my_annotation"
    payload = {
        "name": "My Annotation",
        "type": "point",
        "coordinates": [1024, 2048],
        "reference_id": slide_id,
        "reference_type": "wsi",
        "npp_created": 499,
    }
    response = requests.post(url, headers=headers, json=payload, timeout=5)
    assert response.status_code == 200
    annotation = response.json()
    assert "id" in annotation
    assert annotation["name"] == "My Annotation"
    assert annotation["type"] == "point"
    assert annotation["coordinates"] == [1024, 2048]
    assert annotation["creator_id"] == job_id
    assert annotation["creator_type"] == "job"
    assert annotation["reference_id"] == slide_id
    assert annotation["reference_type"] == "wsi"
    assert annotation["npp_created"] == 499

    # collection
    url = f"{as_url}/v0/{job_id}/outputs/my_collection"
    payload = {
        "name": "My Collection",
        "reference_id": slide_id,
        "reference_type": "wsi",
        "item_type": "integer",
        "items": [
            {
                "name": "My First Integer",
                "type": "integer",
                "value": 42,
            },
            {
                "name": "My Second Integer",
                "type": "integer",
                "value": 43,
            },
        ],
    }
    response = requests.post(url, headers=headers, json=payload, timeout=5)
    assert response.status_code == 200
    collection = response.json()
    assert "id" in collection
    assert collection["name"] == "My Collection"
    assert collection["type"] == "collection"
    assert collection["creator_id"] == job_id
    assert collection["creator_type"] == "job"
    assert "id" in collection["items"][0]
    assert collection["items"][0]["name"] == "My First Integer"
    assert collection["items"][0]["type"] == "integer"
    assert collection["items"][0]["creator_id"] == job_id
    assert collection["items"][0]["creator_type"] == "job"
    assert collection["items"][0]["value"] == 42
    assert "id" in collection["items"][1]
    assert collection["items"][1]["name"] == "My Second Integer"
    assert collection["items"][1]["type"] == "integer"
    assert collection["items"][1]["creator_id"] == job_id
    assert collection["items"][1]["creator_type"] == "job"
    assert collection["items"][1]["value"] == 43

    # invalid output
    url = f"{as_url}/v0/{job_id}/outputs/invalid_output"
    response = requests.post(url, headers=headers, json=payload, timeout=5)
    assert response.status_code == 404
