import requests

from ..singletons import as_url, mds_url
from .helper import create_running_job


def test_set_finalized():
    job_token = create_running_job(
        inputs_spec={},
        outputs_spec={
            "my_primitive": {"type": "integer"},
        },
        inputs={},
    )
    job_id = job_token["job_id"]
    access_token = job_token["access_token"]
    headers = {"Authorization": f"Bearer {access_token}"}

    # finalization of app not possible with missing outputs
    url = f"{as_url}/v0/{job_id}/finalize"
    response = requests.put(url, headers=headers, timeout=5)
    assert response.status_code == 409

    # set missing output
    my_primitive = {
        "name": "My Integer",
        "type": "integer",
        "value": 42,
    }
    url = f"{as_url}/v0/{job_id}/outputs/my_primitive"
    response = requests.post(url, headers=headers, json=my_primitive, timeout=5)
    assert response.status_code == 200
    my_primitive = response.json()

    # finalization of app now possible
    url = f"{as_url}/v0/{job_id}/finalize"
    response = requests.put(url, headers=headers, timeout=5)
    assert response.status_code == 200

    # ensure finalization was recognized internally
    url = f"{mds_url}/v1/jobs/{job_id}"
    response = requests.get(url, timeout=5)
    assert response.status_code == 200
    job = response.json()
    assert job["status"] == "COMPLETED"

    # ensure output has been locked internally
    my_primitive_id = my_primitive["id"]
    url = f"{mds_url}/v1/primitives/{my_primitive_id}"
    response = requests.get(url, timeout=5)
    assert response.status_code == 200
    my_primitive = response.json()
    assert my_primitive["is_locked"]

    # prevent from promoting finalization again
    url = f"{as_url}/v0/{job_id}/finalize"
    response = requests.put(url, headers=headers, timeout=5)
    assert response.status_code == 400
