import requests

from ..singletons import as_url
from .helper import add_app_config, create_portal_app, create_running_job


def test_get_configuration():
    portal_app = create_portal_app()
    app = portal_app["active_app_views"]["v1"]["app"]
    config_ground_truth = {}
    for k in list(app["ead"]["configuration"].keys()):
        value = None
        if app["ead"]["configuration"][k]["type"] == "integer":
            value = 42
        if app["ead"]["configuration"][k]["type"] == "float":
            value = 42.0
        if app["ead"]["configuration"][k]["type"] == "string":
            value = "42"
        if app["ead"]["configuration"][k]["type"] == "bool":
            value = True
        config_ground_truth[k] = value
    add_app_config(app["id"], config_ground_truth)

    job_token = create_running_job(
        inputs_spec=app["ead"]["inputs"],
        outputs_spec=app["ead"]["outputs"],
        inputs={},
        app_id=app["id"],
    )
    job_id = job_token["job_id"]
    access_token = job_token["access_token"]
    headers = {"Authorization": f"Bearer {access_token}"}

    url = f"{as_url}/v0/{job_id}/configuration"
    response = requests.get(url, headers=headers, timeout=5)
    assert response.status_code == 200
    config = response.json()
    assert config == config_ground_truth
