import uuid

import requests

from ..singletons import as_url
from .helper import create_running_job, create_slide_in_case


def test_query_collection():
    slide_id = create_slide_in_case("Aperio/CMU-1.svs")
    other_slide_id = str(uuid.uuid4())
    job_token = create_running_job(
        inputs_spec={
            "my_slide": {"type": "wsi"},
            "my_collection": {"type": "collection", "items": {"type": "integer"}},
        },
        outputs_spec={},
        inputs={
            "my_slide": {
                "type": "wsi",
                "slide_id": slide_id,
            },
            "my_collection": {
                "name": "My Collection",
                "type": "collection",
                "item_type": "integer",
                "items": [
                    {
                        "name": "My First Integer",
                        "type": "integer",
                        "value": 42,
                        "reference_id": slide_id,
                        "reference_type": "wsi",
                    },
                    {
                        "name": "My Second Integer",
                        "type": "integer",
                        "value": 43,
                        "reference_id": other_slide_id,
                        "reference_type": "wsi",
                    },
                ],
            },
        },
    )
    job_id = job_token["job_id"]
    access_token = job_token["access_token"]
    headers = {"Authorization": f"Bearer {access_token}"}

    # find out collection id via input parameter
    url = f"{as_url}/v0/{job_id}/inputs/my_collection"
    response = requests.get(url, headers=headers, timeout=5)
    assert response.status_code == 200
    collection = response.json()
    collection_id = collection["id"]

    # query for items that reference given slide
    query = {"references": [slide_id]}
    url = f"{as_url}/v0/{job_id}/collections/{collection_id}/query"
    response = requests.put(url, headers=headers, json=query, timeout=5)
    assert response.status_code == 200
    item_query_list = response.json()
    assert item_query_list["item_count"] == 1
    assert item_query_list["items"][0]["name"] == "My First Integer"
    assert item_query_list["items"][0]["type"] == "integer"
    assert item_query_list["items"][0]["value"] == 42
    assert item_query_list["items"][0]["reference_id"] == slide_id
    assert item_query_list["items"][0]["reference_type"] == "wsi"

    # invalid collection id
    invalid_collection_id = str(uuid.uuid4())
    url = f"{as_url}/v0/{job_id}/collections/{invalid_collection_id}/query"
    query = {}
    response = requests.put(url, headers=headers, json=query, timeout=5)
    assert response.status_code == 404
