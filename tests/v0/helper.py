import json
import uuid

import requests

from ..singletons import mds_url, mpsm_url


def create_portal_app():
    # create
    portal_app = _generate_portal_app()
    url = f"{mpsm_url}/v1/custom-mock/portal-apps"
    response = requests.post(url, json=portal_app, timeout=5)
    print(response.status_code, response.text)
    assert response.status_code == 200
    app_id = portal_app["active_app_views"]["v1"]["app"]["id"]
    # test via get by app id
    url = f"{mpsm_url}/v1/customer/apps/{app_id}"
    response = requests.get(url, timeout=5, headers={"organization-id": str(uuid.uuid4)})
    assert response.status_code == 200
    return portal_app


def create_running_job(inputs_spec, outputs_spec, inputs, config_spec=None, app_id=None):
    job_token = create_assembly_job(inputs_spec, outputs_spec, inputs, config_spec, app_id)
    _set_job_running(job_token["job_id"])
    return job_token


def create_assembly_job(inputs_spec, outputs_spec, inputs, config_spec=None, app_id=None):
    app_id = app_id or str(uuid.uuid4())
    creator_id = str(uuid.uuid4())
    url = f"{mds_url}/v1/apps/{app_id}/ead"
    ead = _generate_ead(inputs_spec, outputs_spec, config_spec)
    response = requests.put(url, json=ead, timeout=5)
    assert response.status_code == 200 or response.status_code == 400
    job = {
        "app_id": app_id,
        "creator_id": creator_id,
        "creator_type": "SCOPE",
    }
    url = f"{mds_url}/v1/jobs"
    response = requests.post(url, json=job, timeout=5)
    assert response.status_code == 200
    job = response.json()
    job_id = job["id"]
    for input_key, input_payload in inputs.items():
        _create_job_input(job_id, input_key, input_payload, creator_id)
    url = f"{mds_url}/v1/jobs/{job_id}/token"
    response = requests.get(url, timeout=5)
    assert response.status_code == 200
    return response.json()


def add_app_config(app_id, config):
    url = f"{mpsm_url}/v1/custom-mock/apps/{app_id}/config/global"
    response = requests.put(url, json=config, timeout=5)
    assert response.status_code == 200


def create_slide_in_case(storage_address):
    case_id = create_case()
    slide_id = create_slide(case_id)
    create_storage(storage_address, slide_id)
    return slide_id


def create_case():
    case_ = {
        "creator_id": str(uuid.uuid4()),
        "creator_type": "USER",
    }
    url = f"{mds_url}/private/v1/cases"
    response = requests.post(url, json=case_, timeout=5)
    assert response.status_code == 200
    case_ = response.json()
    return case_["id"]


def create_slide(case_id):
    slide = {"case_id": case_id}
    url = f"{mds_url}/private/v1/slides"
    response = requests.post(url, json=slide, timeout=5)
    assert response.status_code == 200
    slide = response.json()
    return slide["id"]


def create_storage(storage_adress, slide_id):
    url = f"{mds_url}/private/v1/slides/{slide_id}/storage"
    slide_storage_info = {"main_storage_address": {"storage_address_id": str(uuid.uuid4()), "path": storage_adress}}
    r = requests.put(url, data=json.dumps(slide_storage_info), timeout=5)
    print(r.content)
    assert r.status_code == 200


def _create_job_input(job_id, input_key, payload, creator_id):
    resource = _create_resource(payload, creator_id)
    _set_job_input(job_id, input_key, resource["id"])


def _create_resource(payload, creator_id):
    _set_creator_id_and_type(payload, creator_id)
    if payload["type"] == "wsi":
        return {"id": payload["slide_id"]}
    elif payload["type"] in ("integer", "float", "bool", "string"):
        return _create_primitive(payload)
    elif payload["type"] in ("line", "arrow", "rectangle", "point", "circle", "polygon"):
        return _create_annotation(payload)
    elif payload["type"] == "collection":
        return _create_collection(payload)
    else:
        raise NotImplementedError()


def _set_creator_id_and_type(payload, creator_id):
    if payload["type"] == "collection":
        for item in payload["items"]:
            _set_creator_id_and_type(item, creator_id)
    if payload["type"] != "wsi":
        payload["creator_id"] = creator_id
        payload["creator_type"] = "scope"


def _create_primitive(payload):
    url = f"{mds_url}/v1/primitives"
    return _post_payload(url, payload)


def _create_annotation(payload):
    url = f"{mds_url}/v1/annotations"
    return _post_payload(url, payload)


def _create_collection(payload):
    url = f"{mds_url}/v1/collections"
    return _post_payload(url, payload)


def _post_payload(url, payload):
    response = requests.post(url, json=payload, timeout=5)
    print(response.status_code, response.text)
    assert response.status_code == 201
    return response.json()


def _set_job_input(job_id, input_key, input_id):
    url = f"{mds_url}/v1/jobs/{job_id}/inputs/{input_key}"
    response = requests.put(url, json={"id": input_id}, timeout=5)
    assert response.status_code == 200


def _set_job_running(job_id):
    url = f"{mds_url}/v1/jobs/{job_id}/status"
    response = requests.put(url, json={"status": "RUNNING"}, timeout=5)
    assert response.status_code == 200


def _generate_ead(inputs, outputs, config=None):
    ead = {
        "$schema": "https://developer.empaia.org/schema/ead-app-schema-draft-3.json",
        "name": "Test App",
        "name_short": "TestApp",
        "namespace": "org.empaia.vendor_name.ta.v1",
        "description": "EAD for testing purposes",
        "inputs": inputs,
        "outputs": outputs,
    }
    if config:
        ead["configuration"] = config
    return ead


def _generate_portal_app():
    v1_image_url = "registry.gitlab.com/empaia/integration/sample-apps/v1/org-empaia-vendor_name-tutorial_app_07:v1"
    v2_image_url = "registry.gitlab.com/empaia/integration/sample-apps/v2/org-empaia-vendor_name-tutorial_app_07:v2"
    v3_image_url = "registry.gitlab.com/empaia/integration/sample-apps/v3/org-empaia-vendor_name-tutorial_app_07:v3.0"
    v1v2_schema_url = "https://gitlab.com/empaia/integration/definitions/-/raw/main/ead/ead-schema.v1-draft3.json"
    v3_schema_url = "https://gitlab.com/empaia/integration/definitions/-/raw/main/ead/ead-schema.v3.json"
    return {
        "active_app_views": {
            "v1": {
                "api_version": "v1",
                "app": {
                    "app_ui_url": None,
                    "created_at": 1691683624,
                    "creator_id": "c82d512f-bf74-475a-b3da-b0cadc4fc595",
                    "ead": {
                        "$schema": v1v2_schema_url,
                        "configuration": {
                            "optional_parameter": {"optional": True, "storage": "global", "type": "integer"},
                            "private_api_password": {"optional": False, "storage": "global", "type": "string"},
                            "private_api_username": {"optional": False, "storage": "global", "type": "string"},
                        },
                        "data_transmission_to_external_service_provider": True,
                        "description": "Human readable " "description",
                        "inputs": {
                            "my_rectangle": {"reference": "inputs.my_wsi", "type": "rectangle"},
                            "my_wsi": {"type": "wsi"},
                        },
                        "name": "Tutorial App 07 v1",
                        "name_short": "TA07v1",
                        "namespace": "org.empaia.vendor_name.tutorial_app_07.v1",
                        "outputs": {"tumor_cell_count": {"type": "integer"}},
                    },
                    "has_frontend": False,
                    "id": "08c33ecd-c02d-4c58-951b-69aacfafd2c8",
                    "portal_app_id": "e7cd625e-7032-446d-a759-e9c58491999e",
                    "registry_image_url": v1_image_url,
                    "status": "APPROVED",
                    "updated_at": 1691683624,
                    "version": "v1",
                },
                "created_at": 1691683624,
                "creator_id": "c82d512f-bf74-475a-b3da-b0cadc4fc595",
                "details": {
                    "description": [
                        {"lang": "EN", "text": "Human " "readable " "description"},
                        {"lang": "DE", "text": "Human " "readable " "description"},
                    ],
                    "marketplace_url": "http://url.to/store",
                    "name": "org.empaia.vendor_name.tutorial_app_07.v1",
                },
                "id": "91e02eb6-5ec6-4601-b8dd-2582ebb99aca",
                "media": {
                    "peek": [
                        {
                            "alt_text": [
                                {"lang": "EN", "text": "Peek " "alternative " "text"},
                                {"lang": "DE", "text": "Alternativtext " "Übersichtsbild"},
                            ],
                            "caption": [
                                {"lang": "EN", "text": "Peek " "caption"},
                                {"lang": "DE", "text": "Titel " "Übersichtsbild"},
                            ],
                            "content_type": "image/jpeg",
                            "id": "342a65f8-1ca9-4623-bac5-f1608b5d7f2c",
                            "index": 0,
                            "internal_path": "/internal/path/to",
                            "presigned_media_url": "https://url.to/image",
                        }
                    ]
                },
                "non_functional": False,
                "organization_id": "75909528-2525-4dd0-b6d7-eb258f52f843",
                "portal_app_id": "e7cd625e-7032-446d-a759-e9c58491999e",
                "review_comment": "Review comment",
                "reviewer_id": "eaf6ec22-c1a0-4782-8d6b-bc0b69cbb0a5",
                "status": "APPROVED",
                "tags": {
                    "analysis": [],
                    "indications": [],
                    "stains": [],
                    "tissues": [
                        {
                            "name": "SKIN",
                            "tag_translations": [{"lang": "EN", "text": "Skin"}, {"lang": "DE", "text": "Haut"}],
                        }
                    ],
                },
                "version": "v1",
            },
            "v2": {
                "api_version": "v2",
                "app": {
                    "app_ui_url": None,
                    "created_at": 1691683624,
                    "creator_id": "79dbc698-240c-47c5-830c-e0fa3ce71100",
                    "ead": {
                        "$schema": v1v2_schema_url,
                        "configuration": {
                            "optional_parameter": {"optional": True, "storage": "global", "type": "integer"},
                            "private_api_password": {"optional": False, "storage": "global", "type": "string"},
                            "private_api_username": {"optional": False, "storage": "global", "type": "string"},
                        },
                        "data_transmission_to_external_service_provider": True,
                        "description": "Human readable " "description",
                        "inputs": {
                            "my_rectangle": {"reference": "inputs.my_wsi", "type": "rectangle"},
                            "my_wsi": {"type": "wsi"},
                        },
                        "name": "Tutorial App 07 v2",
                        "name_short": "TA07v2",
                        "namespace": "org.empaia.vendor_name.tutorial_app_07.v2",
                        "outputs": {"tumor_cell_count": {"type": "integer"}},
                    },
                    "has_frontend": False,
                    "id": "75506c8e-900d-48f1-be71-5eadedb498e1",
                    "portal_app_id": "8c4eb21b-1f37-409c-bf6d-3fc3959cc593",
                    "registry_image_url": v2_image_url,
                    "status": "APPROVED",
                    "updated_at": 1691683624,
                    "version": "v2",
                },
                "created_at": 1691683624,
                "creator_id": "79dbc698-240c-47c5-830c-e0fa3ce71100",
                "details": {
                    "description": [
                        {"lang": "EN", "text": "Human " "readable " "description"},
                        {"lang": "DE", "text": "Human " "readable " "description"},
                    ],
                    "marketplace_url": "http://url.to/store",
                    "name": "org.empaia.vendor_name.tutorial_app_07.v2",
                },
                "id": "55abb2d0-0ae5-473a-b8d1-2857f6f404e6",
                "media": {
                    "peek": [
                        {
                            "alt_text": [
                                {"lang": "EN", "text": "Peek " "alternative " "text"},
                                {"lang": "DE", "text": "Alternativtext " "Übersichtsbild"},
                            ],
                            "caption": [
                                {"lang": "EN", "text": "Peek " "caption"},
                                {"lang": "DE", "text": "Titel " "Übersichtsbild"},
                            ],
                            "content_type": "image/jpeg",
                            "id": "d8b757aa-446d-4a2f-82a3-db577e4056ce",
                            "index": 0,
                            "internal_path": "/internal/path/to",
                            "presigned_media_url": "https://url.to/image",
                        }
                    ]
                },
                "non_functional": False,
                "organization_id": "75909528-2525-4dd0-b6d7-eb258f52f843",
                "portal_app_id": "8c4eb21b-1f37-409c-bf6d-3fc3959cc593",
                "review_comment": "Review comment",
                "reviewer_id": "f063caf1-e7d0-49c1-bb10-45d41660d252",
                "status": "APPROVED",
                "tags": {
                    "analysis": [],
                    "indications": [],
                    "stains": [],
                    "tissues": [
                        {
                            "name": "SKIN",
                            "tag_translations": [{"lang": "EN", "text": "Skin"}, {"lang": "DE", "text": "Haut"}],
                        }
                    ],
                },
                "version": "v2",
            },
            "v3": {
                "api_version": "v3",
                "app": {
                    "app_ui_url": None,
                    "created_at": 1691683624,
                    "creator_id": "235305d2-e2ef-470e-8b80-9a95eedb59dd",
                    "ead": {
                        "$schema": v3_schema_url,
                        "configuration": {
                            "global": {
                                "optional_parameter": {"optional": True, "type": "integer"},
                                "private_api_password": {"optional": False, "type": "string"},
                                "private_api_username": {"optional": False, "type": "string"},
                            }
                        },
                        "description": "Human readable " "description",
                        "io": {
                            "my_rectangle": {"reference": "io.my_wsi", "type": "rectangle"},
                            "my_wsi": {"type": "wsi"},
                            "tumor_cell_count": {"type": "integer"},
                        },
                        "modes": {
                            "standalone": {"inputs": ["my_wsi", "my_rectangle"], "outputs": ["tumor_cell_count"]}
                        },
                        "name": "Tutorial App 07 v3",
                        "name_short": "TA07v3",
                        "namespace": "org.empaia.vendor_name.tutorial_app_07.v3.0",
                        "permissions": {"data_transmission_to_external_service_provider": True},
                    },
                    "has_frontend": False,
                    "id": "cfb01adb-b5f3-4834-8257-dce8568b0e09",
                    "portal_app_id": "872f4553-9db5-43d3-b2fd-5210fa913fb5",
                    "registry_image_url": v3_image_url,
                    "status": "APPROVED",
                    "updated_at": 1691683624,
                    "version": "v3.0",
                },
                "created_at": 1691683624,
                "creator_id": "235305d2-e2ef-470e-8b80-9a95eedb59dd",
                "details": {
                    "description": [
                        {"lang": "EN", "text": "Human " "readable " "description"},
                        {"lang": "DE", "text": "Human " "readable " "description"},
                    ],
                    "marketplace_url": "http://url.to/store",
                    "name": "org.empaia.vendor_name.tutorial_app_07.v3.0",
                },
                "id": "7e45288b-084f-4c35-80e2-aefab3a6e8a6",
                "media": {
                    "peek": [
                        {
                            "alt_text": [
                                {"lang": "EN", "text": "Peek " "alternative " "text"},
                                {"lang": "DE", "text": "Alternativtext " "Übersichtsbild"},
                            ],
                            "caption": [
                                {"lang": "EN", "text": "Peek " "caption"},
                                {"lang": "DE", "text": "Titel " "Übersichtsbild"},
                            ],
                            "content_type": "image/jpeg",
                            "id": "055e4d34-eecf-470b-b01e-31484f1b0cfb",
                            "index": 0,
                            "internal_path": "/internal/path/to",
                            "presigned_media_url": "https://url.to/image",
                        }
                    ]
                },
                "non_functional": False,
                "organization_id": "75909528-2525-4dd0-b6d7-eb258f52f843",
                "portal_app_id": "872f4553-9db5-43d3-b2fd-5210fa913fb5",
                "review_comment": "Review comment",
                "reviewer_id": "7a28812d-9a81-4635-a920-866e80dfc44b",
                "status": "APPROVED",
                "tags": {
                    "analysis": [],
                    "indications": [],
                    "stains": [],
                    "tissues": [
                        {
                            "name": "SKIN",
                            "tag_translations": [{"lang": "EN", "text": "Skin"}, {"lang": "DE", "text": "Haut"}],
                        }
                    ],
                },
                "version": "v3.0",
            },
        },
        "created_at": 1691683624,
        "creator_id": "235305d2-e2ef-470e-8b80-9a95eedb59dd",
        "id": "872f4553-9db5-43d3-b2fd-5210fa913fb5",
        "organization_id": "75909528-2525-4dd0-b6d7-eb258f52f843",
        "status": "LISTED",
        "updated_at": 1691683624,
    }
