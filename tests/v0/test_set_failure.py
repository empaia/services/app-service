import requests

from ..singletons import as_url, mds_url
from .helper import create_running_job


def test_set_failure():
    job_token = create_running_job(
        inputs_spec={},
        outputs_spec={},
        inputs={},
    )
    job_id = job_token["job_id"]
    access_token = job_token["access_token"]
    headers = {"Authorization": f"Bearer {access_token}"}

    # promote failure of app
    url = f"{as_url}/v0/{job_id}/failure"
    user_message = "the app failed for testing purposes"
    response = requests.put(url, headers=headers, json={"user_message": user_message}, timeout=5)
    assert response.status_code == 200

    # ensure it was recognized internally
    url = f"{mds_url}/v1/jobs/{job_id}"
    response = requests.get(url, timeout=5)
    assert response.status_code == 200
    job = response.json()
    assert "error_message" in job
    assert job["error_message"] == user_message
    assert job["status"] == "ERROR"

    # prevent from promoting failure again
    url = f"{as_url}/v0/{job_id}/failure"
    new_user_message = "the app failed again and again"
    response = requests.put(url, headers=headers, json={"user_message": new_user_message}, timeout=5)
    assert response.status_code == 400
    url = f"{mds_url}/v1/jobs/{job_id}"
    response = requests.get(url, timeout=5)
    assert response.status_code == 200
    job = response.json()
    assert "error_message" in job
    assert job["error_message"] == user_message
    assert job["status"] == "ERROR"
