import requests

from ..singletons import as_url, mds_url
from .helper import create_running_job, create_slide_in_case


def test_get_region():
    slide_id = create_slide_in_case("Aperio/CMU-1.svs")
    job_token = create_running_job(
        inputs_spec={"my_slide": {"type": "wsi"}},
        outputs_spec={},
        inputs={
            "my_slide": {
                "type": "wsi",
                "slide_id": slide_id,
            }
        },
    )
    job_id = job_token["job_id"]
    access_token = job_token["access_token"]
    headers = {"Authorization": f"Bearer {access_token}"}

    level = 0
    start_x = 30000
    start_y = 7000
    size_x = 64
    size_y = 64
    image_format = "png"
    image_quality = 90
    z_index = 0

    url = f"{as_url}/v0/{job_id}/regions/{slide_id}"
    url += f"/level/{level}/start/{start_x}/{start_y}"
    url += f"/size/{size_x}/{size_y}?image_format={image_format}"
    url += f"&image_quality={image_quality}&z={z_index}"
    response = requests.get(url, headers=headers, timeout=5)
    assert response.status_code == 200

    ref_url = f"{mds_url}/v1/slides/{slide_id}/region"
    ref_url += f"/level/{level}/start/{start_x}/{start_y}"
    ref_url += f"/size/{size_x}/{size_y}?image_format={image_format}"
    ref_url += f"&image_quality={image_quality}&z={z_index}"
    ref_response = requests.get(ref_url, timeout=5)
    assert ref_response.status_code == 200
    assert response.content == ref_response.content

    # invalid slide id
    url = f"{as_url}/v0/{job_id}/regions/some-invalid-id"
    url += f"/level/{level}/start/{start_x}/{start_y}"
    url += f"/size/{size_x}/{size_y}?image_format={image_format}"
    url += f"&image_quality={image_quality}&z={z_index}"
    response = requests.get(url, headers=headers, timeout=5)
    assert response.status_code == 404
