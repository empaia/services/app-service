import requests

from ..singletons import as_url
from .helper import create_running_job, create_slide_in_case


def test_get_input():
    slide_id = create_slide_in_case("Aperio/CMU-1.svs")
    job_token = create_running_job(
        inputs_spec={
            "my_slide": {"type": "wsi"},
            "my_primitive": {"type": "integer"},
            "my_annotation": {"type": "point", "reference": "inputs.my_slide"},
            "my_collection": {"type": "collection", "items": {"type": "integer"}},
        },
        outputs_spec={},
        inputs={
            "my_slide": {
                "type": "wsi",
                "slide_id": slide_id,
            },
            "my_primitive": {
                "name": "My Primitive",
                "type": "integer",
                "value": 42,
            },
            "my_annotation": {
                "name": "My Annotation",
                "type": "point",
                "reference_id": slide_id,
                "reference_type": "wsi",
                "coordinates": [1024, 2048],
                "npp_created": 499,
            },
            "my_collection": {
                "name": "My Collection",
                "type": "collection",
                "item_type": "integer",
                "items": [
                    {
                        "name": "My First Integer",
                        "type": "integer",
                        "value": 42,
                    },
                    {
                        "name": "My Second Integer",
                        "type": "integer",
                        "value": 43,
                    },
                ],
            },
        },
    )
    job_id = job_token["job_id"]
    access_token = job_token["access_token"]
    headers = {"Authorization": f"Bearer {access_token}"}

    # slide
    url = f"{as_url}/v0/{job_id}/inputs/my_slide"
    response = requests.get(url, headers=headers, timeout=5)
    assert response.status_code == 200
    slide = response.json()
    assert slide["channel_depth"] == 8
    assert slide["num_levels"] == 3
    assert slide["extent"] == {"x": 46000, "y": 32914, "z": 1}

    # primitive
    url = f"{as_url}/v0/{job_id}/inputs/my_primitive"
    response = requests.get(url, headers=headers, timeout=5)
    assert response.status_code == 200
    primitive = response.json()
    assert primitive["name"] == "My Primitive"
    assert primitive["type"] == "integer"
    assert primitive["value"] == 42

    # annotation
    url = f"{as_url}/v0/{job_id}/inputs/my_annotation"
    response = requests.get(url, headers=headers, timeout=5)
    assert response.status_code == 200
    annotation = response.json()
    assert annotation["name"] == "My Annotation"
    assert annotation["type"] == "point"
    assert annotation["coordinates"] == [1024, 2048]

    # collection
    url = f"{as_url}/v0/{job_id}/inputs/my_collection"
    response = requests.get(url, headers=headers, timeout=5)
    assert response.status_code == 200
    collection = response.json()
    assert collection["name"] == "My Collection"
    assert collection["type"] == "collection"
    assert collection["item_type"] == "integer"
    assert collection["item_count"] == 2
    assert collection["items"][0]["name"] == "My First Integer"
    assert collection["items"][0]["type"] == "integer"
    assert collection["items"][0]["value"] == 42
    assert collection["items"][1]["name"] == "My Second Integer"
    assert collection["items"][1]["type"] == "integer"
    assert collection["items"][1]["value"] == 43

    # invalid input
    url = f"{as_url}/v0/{job_id}/inputs/invalid_input"
    response = requests.get(url, headers=headers, timeout=5)
    assert response.status_code == 404
