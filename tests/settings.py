from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    as_url: str
    mds_url: str
    mpsm_url: str

    model_config = SettingsConfigDict(env_prefix="pytest_", env_file=".env", extra="ignore")
