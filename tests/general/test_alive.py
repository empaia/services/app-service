import requests

from app_service import __version__

from ..singletons import as_url


def test_alive():
    url = f"{as_url}/alive"
    response = requests.get(url, timeout=5)
    assert response.status_code == 200
    service_status = response.json()
    assert service_status["status"] == "ok"
    assert service_status["version"] == __version__
