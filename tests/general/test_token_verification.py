import json

import requests

from ..singletons import as_url

API_VERSIONS = ["v0", "v3"]
EXCLUDED_PATHS = []


def test_token_verification():
    for api_version in API_VERSIONS:
        r = requests.get(f"{as_url}/{api_version}/openapi.json", timeout=5)
        openapi = json.loads(r.content)
        paths = openapi["paths"].items()
        assert len(paths) > 3
        for path, ops in paths:
            if path in EXCLUDED_PATHS:
                continue

            path_no_vars = path.replace("{", "").replace("}", "")
            url = f"{as_url}/{api_version}{path_no_vars}"
            for op in ops.keys():
                r = requests.request(op, url, timeout=5)
                assert r.status_code == 403
