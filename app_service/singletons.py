import logging

from app_service.simple_cache import SimpleCache

from .empaia_sender_auth import AioHttpClient, AuthSettings
from .settings import Settings

settings = Settings()

logger = logging.getLogger("uvicorn")
logger.setLevel(logging.INFO)
if settings.debug:
    logger.setLevel(logging.DEBUG)

mds_url = settings.mds_url.rstrip("/")
mps_url = settings.mps_url.rstrip("/")

if settings.idp_url:
    auth_settings = AuthSettings(
        idp_url=settings.idp_url,
        client_id=settings.client_id,
        client_secret=settings.client_secret,
    )
else:
    auth_settings = None

http_client = AioHttpClient(
    logger=logger,
    timeout=settings.http_client_timeout,
    request_timeout=settings.request_timeout,
    auth_settings=auth_settings,
    chunk_size=settings.connection_chunk_size,
    connection_limit_per_host=settings.connection_limit_per_host,
)

v3_job_case_cache = SimpleCache()
