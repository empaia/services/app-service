from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    """Parameters to be loaded from Environment Variables in .env file"""

    idp_url: str = ""
    mds_url: str = "http://medical-data-service:8000"
    mps_url: str = "http://marketplace-service:8000"
    client_id: str = ""
    client_secret: str = ""
    debug: bool = False
    enable_token_verification: bool = False
    http_client_timeout: int = 300
    request_timeout: int = 300
    connection_limit_per_host: int = 100
    connection_chunk_size: int = 1024000
    root_path: str = ""
    organization_id: str = "dummy"
    enable_profiling: bool = False
    enable_annot_case_data_partitioning: bool = False

    model_config = SettingsConfigDict(env_prefix="as_", env_file=".env", extra="ignore")
