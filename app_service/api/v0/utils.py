import asyncio

from fastapi import HTTPException

from ...singletons import logger


async def delayed_logging(message):
    logger.error(message)


def raise_http_exception(status_code, detail):
    asyncio.create_task(delayed_logging(f"Details: {detail}"))
    raise HTTPException(status_code=status_code, detail=detail)


def add_attributes(core_resource, job_id):
    if core_resource.get("reference_id"):
        # Class reference_id is a UUID type (even after .dict()
        core_resource["reference_id"] = str(core_resource["reference_id"])

    if "type" not in core_resource:
        if "items" in core_resource:
            core_resource["type"] = "collection"
        elif "value" in core_resource:
            core_resource["type"] = "class"

    if "items" in core_resource:
        for core_item in core_resource["items"]:
            add_attributes(core_item, job_id)

    core_resource["creator_id"] = job_id
    core_resource["creator_type"] = "job"
