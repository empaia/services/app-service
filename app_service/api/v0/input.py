from fastapi import status

from . import utils
from .custom_clients import mds_client
from .custom_models import params
from .custom_models.response import AnyInput


def add_routes_input(app):
    @app.get("/{job_id}/inputs/{input_key}", summary="Query specific input", response_model=AnyInput)
    async def _(
        job_id: str = params.JobId,
        input_key: str = params.InputKey,
        shallow: bool = params.Shallow,
    ):
        """
        Queries the input with the given id defined in the app specification.
        """
        job = await mds_client.get_job(job_id)
        if input_key not in job["inputs"]:
            utils.raise_http_exception(status.HTTP_404_NOT_FOUND, "No such input")
        input_id = job["inputs"][input_key]
        ead = job["ead"]
        input_type = ead["inputs"][input_key]["type"]
        return await mds_client.get_resource(input_type, input_id, shallow)
