from fastapi import status

from . import utils
from .custom_clients import mds_client
from .custom_models import params
from .custom_models.request import ItemQuery, NewItems
from .custom_models.response import ItemQueryList, NewItemsResponse


def add_routes_collection(app):
    @app.put(
        "/{job_id}/collections/{collection_id}/query",
        summary="Queries a specific collection for items",
        response_model=ItemQueryList,
    )
    async def _(
        job_id: str = params.JobId,
        collection_id: str = params.CollectionId,
        skip: int = params.Skip,
        limit: int = params.Limit,
        query: ItemQuery = params.ItemQuery,
    ):
        """
        Queries the collection with the given id for items with special references or viewport appearance.
        """
        # ensure that collection_id is referenced by any input of the job?
        _ = job_id
        return await mds_client.get_filtered_collection(collection_id, query.dict(), skip, limit)

    @app.post(
        "/{job_id}/collections/{collection_id}/items",
        summary="Extend collection",
        response_model=NewItemsResponse,
    )
    async def _(
        job_id: str = params.JobId,
        collection_id: str = params.CollectionId,
        payload: NewItems = params.NewItems,
    ):
        """
        Extend a collection with additional items.
        """
        collection = await mds_client.get_resource("collection", collection_id, shallow=True)
        if collection["creator_id"] != job_id:
            utils.raise_http_exception(
                status.HTTP_403_FORBIDDEN, "Extending collections that were not created by this job is not allowed"
            )

        items = payload.dict()["items"]
        for item in items:
            utils.add_attributes(item, job_id)

        items = await mds_client.add_items_to_collection(collection_id, items)
        return items
