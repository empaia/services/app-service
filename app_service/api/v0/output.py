from fastapi import status

from . import utils
from .custom_clients import mds_client
from .custom_models import params
from .custom_models.request import AnyOutput as AnyOutputRequest
from .custom_models.request import Class, Collection
from .custom_models.response import AnyOutput as AnyOutputResponse


def add_routes_output(app):
    @app.post("/{job_id}/outputs/{output_key}", summary="Set specific output", response_model=AnyOutputResponse)
    async def _(
        job_id: str = params.JobId,
        output_key: str = params.OutputKey,
        payload: AnyOutputRequest = params.Output,
    ):
        """
        Set the output with the given id defined in the app specification.
        """
        job = await mds_client.get_job(job_id)
        ead = job["ead"]
        if output_key not in ead["outputs"]:
            utils.raise_http_exception(status.HTTP_404_NOT_FOUND, f"No such output: {output_key}")

        output_id = job["outputs"].get(output_key)
        output_type = ead["outputs"][output_key]["type"]

        if output_id is not None:
            utils.raise_http_exception(status.HTTP_409_CONFLICT, f"{output_key} was already created")

        output_spec = ead["outputs"][output_key]

        _validate_output_payload(payload, output_spec, job)
        mds_payload = payload.dict()
        utils.add_attributes(mds_payload, job["id"])

        result = await mds_client.create_resource(output_type, mds_payload)
        # discuss race condition: what should be done when the following job update fails?
        await mds_client.update_job(job_id, output_key, result["id"])
        return result


def _validate_output_payload(payload, output_spec, job):
    ead = job["ead"]
    if hasattr(payload, "type") and payload.type != output_spec["type"]:
        _raise_output_type_mismatch(wanted_type=output_spec["type"])
    elif output_spec["type"] == "class":
        if not isinstance(payload, Class):
            _raise_output_type_mismatch(wanted_type="class")
        elif "classes" not in ead:
            _raise_missing_class_spec()
        elif not payload.value.startswith(ead["namespace"] + ".classes."):
            _raise_namespace_mismatch()
        else:
            class_hierarchy = payload.value.split(".classes.")[1].split(".")
            class_spec = ead["classes"]
            for item in class_hierarchy:
                if item not in class_spec:
                    _raise_class_not_existing(item)
                class_spec = class_spec[item]
    elif output_spec["type"] == "collection":
        if not isinstance(payload, Collection):
            _raise_output_type_mismatch(wanted_type="collection")
        for item in payload.items:
            _validate_output_payload(item, output_spec["items"], job)


def _raise_output_type_mismatch(wanted_type):
    utils.raise_http_exception(
        status.HTTP_422_UNPROCESSABLE_ENTITY, f"Output type mismatch, it should be {wanted_type}"
    )


def _raise_missing_class_spec():
    utils.raise_http_exception(
        status.HTTP_500_INTERNAL_SERVER_ERROR,
        "Class specification is missing in the EAD though class defined as output",
    )


def _raise_namespace_mismatch():
    utils.raise_http_exception(status.HTTP_422_UNPROCESSABLE_ENTITY, "Class does not match namespace")


def _raise_class_not_existing(item):
    utils.raise_http_exception(status.HTTP_422_UNPROCESSABLE_ENTITY, f"Class not existing in hierarchy: {item}")
