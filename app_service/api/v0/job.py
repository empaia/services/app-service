from fastapi import status

from . import utils
from .custom_clients import mds_client
from .custom_models import params
from .custom_models.request import Failure


def add_routes_job(app):
    @app.put("/{job_id}/finalize", summary="Finalize job")
    async def _(job_id: str = params.JobId):
        """
        Finalize a job.
        """
        job = await mds_client.get_job(job_id)
        ead = job["ead"]
        for output_key in ead["outputs"]:
            output_id = job["outputs"].get(output_key)
            if not output_id:
                utils.raise_http_exception(status.HTTP_409_CONFLICT, "Outputs missing")
            output_type = ead["outputs"][output_key]["type"]
            await mds_client.lock_resource(output_type, output_id, job["id"])
        await mds_client.finalize_job(job_id)

    @app.put("/{job_id}/failure", summary="Job failure")
    async def _(job_id: str = params.JobId, payload: Failure = params.Failure):
        """
        Job failure.
        """
        await mds_client.job_failure(job_id, payload.user_message)
