from typing import List

from .custom_clients import mds_client
from .custom_models import params
from .custom_models.request import ImageFormat


def add_routes_slide(app):
    @app.get(
        "/{job_id}/regions/{wsi_id}/level/{level}/start/{start_x}/{start_y}/size/{size_x}/{size_y}",
        summary="Query WSI region",
    )
    async def _(
        job_id: str = params.JobId,
        wsi_id: str = params.WsiId,
        level: int = params.ZoomLevel,
        start_x: int = params.StartX,
        start_y: int = params.StartY,
        size_x: int = params.SizeX,
        size_y: int = params.SizeY,
        image_format: ImageFormat = params.ImageFormat,
        image_quality: int = params.ImageQuality,
        image_channels: List[int] = params.ImageChannels,
        z: int = params.ZStack,
    ):
        """
        Queries a specific region of the wsi with the given id using a zoom level, start coordinates, and a region size.
        Note that a lossless format such as PNG does NOT necessarily mean that the original data was stored lossless.
        """
        # ensure that wsi_id is referenced by any input of the job?
        _ = job_id
        return await mds_client.get_wsi_region_stream_response(
            wsi_id, level, start_x, start_y, size_x, size_y, image_format.value, image_quality, image_channels, z
        )

    @app.get("/{job_id}/tiles/{wsi_id}/level/{level}/position/{tile_x}/{tile_y}", summary="Query WSI tile")
    async def _(
        job_id: str = params.JobId,
        wsi_id: str = params.WsiId,
        level: int = params.ZoomLevel,
        tile_x: int = params.TileX,
        tile_y: int = params.TileY,
        image_format: ImageFormat = params.ImageFormat,
        image_quality: int = params.ImageQuality,
        image_channels: List[int] = params.ImageChannels,
        z: int = params.ZStack,
    ):
        """
        Queries a specific tile of the wsi with the given id using a zoom level and indexed position of the tile.
        Note that a lossless format such as PNG does NOT necessarily mean that the original data was stored lossless.
        """
        # ensure that wsi_id is referenced by any input of the job?
        _ = job_id
        return await mds_client.get_wsi_tile_stream_response(
            wsi_id, level, tile_x, tile_y, image_format.value, image_quality, image_channels, z
        )
