from typing import Dict, Union

from pydantic import StrictBool, StrictFloat, StrictInt, StrictStr

from .custom_clients import mds_client, mps_client
from .custom_models import params

SecretsConfiguration = Dict[StrictStr, Union[StrictStr, StrictInt, StrictFloat, StrictBool]]


def add_routes_configuration(app):
    @app.get(
        "/{job_id}/configuration",
        summary="Get secrets configuration",
        response_model=SecretsConfiguration,
        responses={
            200: {
                "description": "The secrets configuration object for the app currently running",
                "content": {
                    "application/json": {
                        "example": {
                            "some_token": "secret-token",
                            "some_flag": True,
                            "some_parameter": 42,
                            "some_other_param": 42.5,
                        }
                    }
                },
            }
        },
    )
    async def _(job_id: str = params.JobId):
        """
        Queries the secrets configuration for the given app which is processing the job.
        """
        job = await mds_client.get_job(job_id)
        return await mps_client.get_configuration(job["app_id"])
