from .collection import add_routes_collection
from .configuration import add_routes_configuration
from .input import add_routes_input
from .job import add_routes_job
from .output import add_routes_output
from .slide import add_routes_slide


def add_routes_v0(app):
    add_routes_configuration(app)
    add_routes_input(app)
    add_routes_output(app)
    add_routes_collection(app)
    add_routes_slide(app)
    add_routes_job(app)
