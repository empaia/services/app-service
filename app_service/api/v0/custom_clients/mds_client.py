from fastapi import status

from .. import utils


class MedicalDataServiceClient:
    _WSI_TYPE = "wsi"
    _COLLECTION_TYPE = "collection"
    _PRIMITIVE_TYPES = ("integer", "float", "bool", "string")
    _ANNOTATION_TYPES = ("point", "line", "arrow", "rectangle", "polygon", "circle")
    _CLASS_TYPE = "class"

    def __init__(self, http_client, mds_url):
        self._http_client = http_client
        self._mds_url = mds_url

    async def get_resource(self, resource_type, resource_id, shallow=False):
        if resource_type == self._WSI_TYPE:
            return await self._get_wsi_info(resource_id)
        elif resource_type in self._ANNOTATION_TYPES:
            return await self._get_annotation(resource_id)
        elif resource_type in self._PRIMITIVE_TYPES:
            return await self._get_primitive(resource_id)
        elif resource_type == self._CLASS_TYPE:
            return await self._get_classification(resource_id)
        elif resource_type == self._COLLECTION_TYPE:
            data = await self._get_collection(resource_id, shallow)
            if not shallow:
                await self._resolve_wsi_items(data)
            return data
        utils.raise_http_exception(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="programming error")

    async def create_resource(self, resource_type, resource):
        if resource_type in self._ANNOTATION_TYPES:
            return await self._create_annotation(resource)
        elif resource_type in self._PRIMITIVE_TYPES:
            return await self._create_primitive(resource)
        elif resource_type == self._CLASS_TYPE:
            return await self._create_classification(resource)
        elif resource_type == self._COLLECTION_TYPE:
            return await self._create_collection(resource)
        utils.raise_http_exception(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="programming error")

    async def lock_resource(self, resource_type, resource_id, job_id):
        if resource_type in self._ANNOTATION_TYPES:
            return await self._lock_resource("annotations", resource_id, job_id)
        elif resource_type in self._PRIMITIVE_TYPES:
            return await self._lock_resource("primitives", resource_id, job_id)
        elif resource_type == self._CLASS_TYPE:
            return await self._lock_resource("classes", resource_id, job_id)
        elif resource_type == self._COLLECTION_TYPE:
            return await self._lock_resource("collections", resource_id, job_id)
        utils.raise_http_exception(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="programming error")

    async def get_job_service_public_key(self):
        return await self._get_resource("/v1/job-service/public-key")

    async def get_job(self, job_id):
        return await self._get_resource(f"/v1/jobs/{job_id}", with_ead="True")

    async def update_job(self, job_id, output_key, output_id):
        return await self._put_resource(f"/v1/jobs/{job_id}/outputs/{output_key}", {"id": output_id})

    async def finalize_job(self, job_id):
        return await self._put_resource(f"/v1/jobs/{job_id}/status", {"status": "COMPLETED"})

    async def job_failure(self, job_id, error_message):
        return await self._put_resource(
            f"/v1/jobs/{job_id}/status", {"status": "ERROR", "error_message": error_message}
        )

    async def get_wsi_region_stream_response(
        self, wsi_id, level, start_x, start_y, size_x, size_y, image_format, image_quality, image_channels, z
    ):
        url = f"/v1/slides/{wsi_id}/region/level/{level}/start/{start_x}/{start_y}/size/{size_x}/{size_y}"
        return await self._create_binary_stream_proxy(
            url, image_format=image_format, image_quality=image_quality, image_channels=image_channels, z=z
        )

    async def get_wsi_tile_stream_response(
        self, wsi_id, level, tile_x, tile_y, image_format, image_quality, image_channels, z
    ):
        url = f"/v1/slides/{wsi_id}/tile/level/{level}/tile/{tile_x}/{tile_y}"
        return await self._create_binary_stream_proxy(
            url, image_format=image_format, image_quality=image_quality, image_channels=image_channels, z=z
        )

    async def _get_annotation(self, annotation_id):
        return await self._get_resource(f"/v1/annotations/{annotation_id}")

    async def _create_annotation(self, annotation):
        return await self._post_resource("/v1/annotations", annotation)

    async def _get_primitive(self, primitive_id):
        return await self._get_resource(f"/v1/primitives/{primitive_id}")

    async def _create_primitive(self, primitve):
        return await self._post_resource("/v1/primitives", primitve)

    async def _get_collection(self, collection_id, shallow):
        return await self._get_resource(f"/v1/collections/{collection_id}", shallow=shallow)

    async def _create_collection(self, collection):
        return await self._post_resource("/v1/collections", collection)

    async def _lock_resource(self, resource_type_plural, resource_id, job_id):
        return await self._put_resource(f"/v1/jobs/{job_id}/lock/{resource_type_plural}/{resource_id}", payload=None)

    async def get_filtered_collection(self, collection_id, query, skip, limit):
        if limit:
            filtered_collection = await self._put_resource(
                f"/v1/collections/{collection_id}/items/query", query, skip=skip, limit=limit
            )
        else:
            filtered_collection = await self._put_resource(
                f"/v1/collections/{collection_id}/items/query", query, skip=skip
            )
        return filtered_collection

    async def add_items_to_collection(self, collection_id, items):
        return await self._post_resource(f"/v1/collections/{collection_id}/items", dict(items=items))

    async def _get_classification(self, classification_id):
        return await self._get_resource(f"/v1/classes/{classification_id}")

    async def _create_classification(self, classification):
        return await self._post_resource("/v1/classes", classification)

    async def _get_wsi_info(self, wsi_id):
        data = await self._get_resource(f"/v1/slides/{wsi_id}/info")
        # Quickfix revert SlideInfo Model for api v0
        if isinstance(data, dict):
            for k in ["format", "raw_download"]:
                try:
                    data.pop(k)
                except KeyError:
                    pass
        return data

    async def _resolve_wsi_items(self, collection):
        if collection["item_type"] == "collection":
            for sub_collection in collection["items"]:
                await self._resolve_wsi_items(sub_collection)
        elif collection["item_type"] == "wsi":
            collection["items"] = [await self._get_wsi_info(id_item["id"]) for id_item in collection["items"]]

    async def _get_resource(self, url, **params):
        return await self._http_client.get(self._mds_url + url, params=params)

    async def _post_resource(self, url, payload, **params):
        return await self._http_client.post(self._mds_url + url, json=payload, params=params)

    async def _put_resource(self, url, payload, **params):
        return await self._http_client.put(self._mds_url + url, json=payload, params=params)

    async def _create_binary_stream_proxy(self, url, **params):
        return await self._http_client.get_stream_response(self._mds_url + url, params=params)
