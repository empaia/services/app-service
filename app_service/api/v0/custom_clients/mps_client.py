class MarketplaceServiceClient:
    def __init__(self, http_client, mps_url, organization_id):
        self._http_client = http_client
        self._organization_id = organization_id
        self._base_url = f"{mps_url}/v1"

    async def get_configuration(self, app_id):
        headers = {"organization-id": self._organization_id}
        url = f"{self._base_url}/customer/apps/{app_id}/config"
        config = await self._http_client.get(url, headers=headers)
        return config["global"]
