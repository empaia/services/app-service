from ....singletons import http_client, mds_url, mps_url, settings
from .mds_client import MedicalDataServiceClient
from .mps_client import MarketplaceServiceClient

mds_client = MedicalDataServiceClient(http_client=http_client, mds_url=mds_url)
mps_client = MarketplaceServiceClient(
    http_client=http_client,
    mps_url=mps_url,
    organization_id=settings.organization_id,
)
