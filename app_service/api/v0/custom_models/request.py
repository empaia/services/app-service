from __future__ import annotations as __annotations__

from enum import Enum
from typing import List, Union

from pydantic import BaseModel, Field, constr

from ...models.v1.annotation import annotations, classes, collections, primitives
from ...models.v1.commons import RestrictedBaseModel

ItemQuery = collections.ItemQuery


class Point(annotations.PointAnnotationCore):
    pass


class Line(annotations.LineAnnotationCore):
    pass


class Arrow(annotations.ArrowAnnotationCore):
    pass


class Circle(annotations.CircleAnnotationCore):
    pass


class Rectangle(annotations.RectangleAnnotationCore):
    pass


class Polygon(annotations.PolygonAnnotationCore):
    pass


class Integer(primitives.IntegerPrimitiveCore):
    pass


class Float(primitives.FloatPrimitiveCore):
    pass


class Bool(primitives.BoolPrimitiveCore):
    pass


class String(primitives.StringPrimitiveCore):
    pass


class Class(classes.ClassCore):
    pass


class Collection(collections.CollectionCore):
    items: Union[
        List[Point],
        List[Line],
        List[Arrow],
        List[Circle],
        List[Rectangle],
        List[Polygon],
        List[Integer],
        List[Float],
        List[Bool],
        List[String],
        List[Class],
        List[Collection],
    ] = Field(description="Items belonging to the collection")


Collection.model_rebuild()


class NewItems(BaseModel):
    items: Union[
        List[Point],
        List[Line],
        List[Arrow],
        List[Circle],
        List[Rectangle],
        List[Polygon],
        List[Integer],
        List[Float],
        List[Bool],
        List[String],
        List[Class],
        List[Collection],
    ] = Field(description="Items that should be added to the collection")


class ImageFormat(str, Enum):
    bmp = "bmp"
    gif = "gif"
    jpeg = "jpeg"
    png = "png"
    tiff = "tiff"


class Failure(RestrictedBaseModel):
    user_message: constr(max_length=255) = Field(..., description="A human-readable error message for the user")


AnyAnnotation = Union[Point, Line, Arrow, Circle, Rectangle, Polygon]

AnyPrimitive = Union[Float, Integer, Bool, String]

AnyOutput = Union[AnyAnnotation, AnyPrimitive, Class, Collection]
