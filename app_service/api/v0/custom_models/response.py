from __future__ import annotations as __annotations__

from typing import Dict, List, Union

from pydantic import (
    BaseModel,
    Field,
    StrictBool,
    StrictFloat,
    StrictInt,
    StrictStr,
    conint,
)

from ...models.v1.annotation import annotations, classes, collections, primitives
from ...models.v1.commons import Message, RestrictedBaseModel
from ...models.v1.slide import SlideInfo


class Point(annotations.PointAnnotation):
    pass


class Line(annotations.LineAnnotation):
    pass


class Arrow(annotations.ArrowAnnotation):
    pass


class Circle(annotations.CircleAnnotation):
    pass


class Rectangle(annotations.RectangleAnnotation):
    pass


class Polygon(annotations.PolygonAnnotation):
    pass


class Integer(primitives.IntegerPrimitive):
    pass


class Float(primitives.FloatPrimitive):
    pass


class Bool(primitives.BoolPrimitive):
    pass


class String(primitives.StringPrimitive):
    pass


Class = classes.Class


class Collection(collections.Collection):
    items: Union[
        List[Point],
        List[Line],
        List[Arrow],
        List[Circle],
        List[Rectangle],
        List[Polygon],
        List[Integer],
        List[Float],
        List[Bool],
        List[String],
        List[Class],
        List[SlideInfo],
        List[Union[Collection]],
    ] = Field(
        description="Items belonging to the collection",
        example=[
            {
                "id": "1ccdf4c0-e3ee-4fac-adcb-8503c57dc5c9",
                "name": "ROI",
                "description": "An ROI created by some user",
                "creator_id": "b10648a7-340d-43fc-a2d9-4d91cc86f33f",
                "creator_type": "user",
                "reference_id": "fb47ffdf-55eb-4438-b237-0a1e1948754c",
                "reference_type": "wsi",
                "type": "rectangle",
                "upper_left": [100, 200],
                "width": 500,
                "height": 400,
                "npp_created": 45.56,
            },
            {
                "id": "9075456a-3500-48cb-b569-71000fb35ca6",
                "name": "ROI",
                "description": "An ROI created by some user",
                "creator_id": "b10648a7-340d-43fc-a2d9-4d91cc86f33f",
                "creator_type": "user",
                "reference_id": "fb47ffdf-55eb-4438-b237-0a1e1948754c",
                "reference_type": "wsi",
                "type": "rectangle",
                "upper_left": [500, 800],
                "width": 500,
                "height": 350,
                "npp_created": 45.56,
            },
            {
                "id": "b21a07c0-c4bf-47ac-8662-2e5a5841f393",
                "name": "ROI",
                "description": "An ROI created by some user",
                "creator_id": "b10648a7-340d-43fc-a2d9-4d91cc86f33f",
                "creator_type": "user",
                "reference_id": "fb47ffdf-55eb-4438-b237-0a1e1948754c",
                "reference_type": "wsi",
                "type": "rectangle",
                "upper_left": [1200, 1600],
                "width": 950,
                "height": 640,
                "npp_created": 45.56,
            },
        ],
    )


Collection.model_rebuild()


class ItemQueryList(RestrictedBaseModel):
    item_count: conint(ge=0) = Field(example=12345, description="Count of items.")
    items: Union[
        List[Point],
        List[Line],
        List[Arrow],
        List[Circle],
        List[Rectangle],
        List[Polygon],
        List[Integer],
        List[Float],
        List[Bool],
        List[String],
        List[Class],
        List[SlideInfo],
        List[Union[Collection]],
    ] = Field(description="Items for the given query")


class NewItemsListResponse(BaseModel):
    item_count: conint(ge=0) = Field(example=12345, description="Count of items.")
    items: Union[
        List[Point],
        List[Line],
        List[Arrow],
        List[Circle],
        List[Rectangle],
        List[Polygon],
        List[Integer],
        List[Float],
        List[Bool],
        List[String],
        List[Class],
        List[Collection],
        List[Collection],
    ] = Field(description="Items that were added to the collection")


NewItemsResponse = Union[Message, NewItemsListResponse]

AnyAnnotation = Union[Point, Line, Arrow, Circle, Rectangle, Polygon]

AnyPrimitive = Union[Integer, Float, Bool, String]

AnyInput = Union[AnyAnnotation, AnyPrimitive, classes.Class, SlideInfo, Collection]

AnyOutput = Union[AnyAnnotation, AnyPrimitive, Class, Collection]

SecretsConfiguration = Dict[StrictStr, Union[StrictStr, StrictInt, StrictFloat, StrictBool]]
