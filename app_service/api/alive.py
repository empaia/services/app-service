from fastapi import status

from .. import __version__ as version


def add_routes_alive(app):
    @app.get("/alive", status_code=status.HTTP_200_OK, summary="Health check")
    def _():
        """
        Let downstream services and tools check whether this service is alive.
        """
        return {"status": "ok", "version": version}
