from fastapi import Request

from ..models.v3.annotation.collections import PostItemListApps
from . import utils
from .custom_clients import mds_client
from .custom_models import params
from .custom_models.request import ItemQuery
from .custom_models.response import ItemQueryList, Message, NewItemsResponse


def add_routes_collection(app):
    @app.put(
        "/{job_id}/collections/{collection_id}/query",
        summary="Queries a specific collection for items",
        responses={200: {"model": ItemQueryList}, 404: {"model": Message, "description": "Collection not found"}},
    )
    async def _(
        job_id: str = params.JobId,
        collection_id: str = params.CollectionId,
        skip: int = params.Skip,
        limit: int = params.Limit,
        query: ItemQuery = params.ItemQuery,
    ):
        """
        Queries the collection with the given id for items with special references or viewport appearance.
        """
        case_id = await utils.get_case_id(job_id)
        return await mds_client.get_filtered_collection(collection_id, query.model_dump(), case_id, skip, limit)

    @app.post(
        "/{job_id}/collections/{collection_id}/items",
        summary="Extend collection",
        responses={
            201: {"model": NewItemsResponse},
            404: {"model": Message, "description": "Collection not found"},
            405: {"model": Message, "description": "Collection is locked"},
        },
        status_code=201,
    )
    async def _(
        request: Request,
        job_id: str = params.JobId,
        collection_id: str = params.CollectionId,
        payload: PostItemListApps = params.PostItemList,
    ):
        """
        Extend a collection with additional items.
        """
        case_id = await utils.get_case_id(job_id)
        return await mds_client.add_items_to_collection(collection_id, request.stream(), case_id)
