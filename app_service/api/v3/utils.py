import asyncio

from fastapi import HTTPException

from ...singletons import logger, settings, v3_job_case_cache
from ..v3.custom_clients import mds_client


async def delayed_logging(message):
    logger.error(message)


def raise_http_exception(status_code, detail):
    asyncio.create_task(delayed_logging(f"Details: {detail}"))
    raise HTTPException(status_code=status_code, detail=detail)


async def get_case_id(job_id):
    if not settings.enable_annot_case_data_partitioning:
        return None

    case_id = v3_job_case_cache.get(job_id)
    if case_id is not None:
        return case_id

    async for examination in mds_client.query_examinations(query={"jobs": [job_id]}):
        case_id = examination["case_id"]
        break

    if case_id is None:
        raise_http_exception(500, "Data integrity error")

    v3_job_case_cache.add(job_id, case_id)
    return case_id
