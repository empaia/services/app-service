from fastapi import Request, status

from . import utils
from .custom_clients import mds_client, mps_client
from .custom_models import params
from .custom_models.request import AnyOutput as AnyOutputRequest
from .custom_models.response import AnyOutput as AnyOutputResponse
from .custom_models.response import Message


def add_routes_output(app):
    @app.post(
        "/{job_id}/outputs/{output_key}",
        summary="Set specific output",
        responses={
            200: {"model": AnyOutputResponse},
            404: {"model": Message, "description": "Output with given key not found"},
            409: {"model": Message, "description": "Output with given key already set"},
        },
    )
    async def _(
        request: Request,
        job_id: str = params.JobId,
        output_key: str = params.OutputKey,
        payload: AnyOutputRequest = params.Output,
    ):
        """
        Set the output with the given id defined in the app specification.
        """
        job = await mds_client.get_job(job_id)
        ead = await mps_client.get_ead(job["app_id"])
        job_mode = job["mode"].lower()
        ead_output_keys = ead["modes"][job_mode]["outputs"]
        if output_key not in ead_output_keys:
            utils.raise_http_exception(status.HTTP_404_NOT_FOUND, f"No such output: {output_key}")

        output_id = job["outputs"].get(output_key)
        output_type = ead["io"][output_key]["type"]

        if output_id is not None:
            utils.raise_http_exception(status.HTTP_409_CONFLICT, f"{output_key} was already created")

        case_id = await utils.get_case_id(job_id)

        result = await mds_client.create_resource(output_type, request.stream(), case_id)
        result_id = result["id"]

        if output_type == "fhir_questionnaire_response":
            result_id = f'{result["id"]}||{result["meta"]["versionId"]}'

        await mds_client.update_job(job_id, output_key, result_id)
        return result
