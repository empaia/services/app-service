from fastapi import status

from . import utils
from .custom_clients import mds_client, mps_client
from .custom_models import params
from .custom_models.response import AnyInput, Message


def add_routes_input(app):
    @app.get(
        "/{job_id}/inputs/{input_key}",
        summary="Query specific input",
        responses={200: {"model": AnyInput}, 404: {"model": Message, "description": "Input with given key not found"}},
    )
    async def _(
        job_id: str = params.JobId,
        input_key: str = params.InputKey,
        shallow: bool = params.Shallow,
    ):
        """
        Queries the input with the given id defined in the app specification.
        """
        job = await mds_client.get_job(job_id)
        if input_key not in job["inputs"]:
            utils.raise_http_exception(status.HTTP_404_NOT_FOUND, "No such input")
        input_id = job["inputs"][input_key]
        ead = await mps_client.get_ead(job["app_id"])
        input_type = ead["io"][input_key]["type"]
        case_id = await utils.get_case_id(job_id)
        return await mds_client.get_resource(input_type, input_id, case_id, shallow)

    @app.get(
        "/{job_id}/inputs/{input_key}/selector",
        summary="Query specific selector relative to input key",
        responses={
            200: {"model": AnyInput},
            404: {"model": Message, "description": "Input key or selector_value has not been set"},
        },
    )
    async def _(
        job_id: str = params.JobId,
        input_key: str = params.InputKey,
    ):
        """
        Returns the selector_value from job input
        """
        selector_value = await mds_client.get_selector_value(job_id, input_key)
        return selector_value
