from fastapi import status

from . import utils
from .custom_clients import mds_client, mps_client
from .custom_models import params
from .custom_models.request import Failure, PutJobProgress
from .custom_models.response import IdObject, JobModeResponse

_FHIR_QUESTIONNAIRE_RESPONSE_TYPE = "fhir_questionnaire_response"


def add_routes_job(app):
    @app.put("/{job_id}/finalize", summary="Finalize job")
    async def _(job_id: str = params.JobId):
        """
        Finalize a job.
        """
        job = await mds_client.get_job(job_id)
        ead = await mps_client.get_ead(job["app_id"])
        job_mode = job["mode"].lower()
        ead_output_keys = ead["modes"][job_mode]["outputs"]
        case_id = await utils.get_case_id(job_id)
        for output_key in ead_output_keys:
            output_id = job["outputs"].get(output_key)
            if not output_id:
                utils.raise_http_exception(status.HTTP_409_CONFLICT, "Outputs missing")
            output_type = ead["io"][output_key]["type"]
            if output_type != _FHIR_QUESTIONNAIRE_RESPONSE_TYPE:
                await mds_client.lock_resource(output_type, output_id, job["id"], case_id)
        # NOTE: as we use stream, we need to return it in order to forward any errors to the client
        # e.g. 400 for calling twice
        return await mds_client.finalize_job(job_id)

    @app.put("/{job_id}/failure", summary="Job failure")
    async def _(job_id: str = params.JobId, payload: Failure = params.Failure):
        """
        Job failure.
        """
        # NOTE: as we use stream, we need to return it in order to forwward any errors to the client
        # e.g. 400 for calling twice
        return await mds_client.job_failure(job_id, payload.user_message)

    @app.get("/{job_id}/mode", summary="Get job mode", response_model=JobModeResponse)
    async def _(job_id: str = params.JobId):
        """
        Queries the mode of the given job.
        """
        job = await mds_client.get_job(job_id)
        return JobModeResponse(mode=job["mode"])

    @app.put("/{job_id}/progress", summary="Update job progress")
    async def _(job_id: str = params.JobId, payload: PutJobProgress = params.PutJobProgress):
        """
        Update the job's progress.
        """
        # NOTE: as we use stream, we need to return it in order to forwward any errors to the client
        # e.g. 400 for calling twice
        return await mds_client.update_job_progress(job_id, payload.progress)

    @app.get("/{job_id}/case", summary="Get case id", response_model=IdObject)
    async def _(job_id: str = params.JobId):
        """
        Queries the case of the given job containing the case id.
        """
        async for examination in mds_client.query_examinations(query={"jobs": [job_id]}):
            return {"id": examination["case_id"]}
        utils.raise_http_exception(status.HTTP_500_INTERNAL_SERVER_ERROR, "Data integrity error")
