from fastapi import status

from .. import utils


class MedicalDataServiceClient:
    _WSI_TYPE = "wsi"
    _COLLECTION_TYPE = "collection"
    _PRIMITIVE_TYPES = ("integer", "float", "bool", "string")
    _ANNOTATION_TYPES = ("point", "line", "arrow", "rectangle", "polygon", "circle")
    _PIXELMAP_TYPES = ("continuous_pixelmap", "discrete_pixelmap", "nominal_pixelmap")
    _CLASS_TYPE = "class"
    _FHIR_QUESTIONNAIRE_TYPE = "fhir_questionnaire"
    _FHIR_QUESTIONNAIRE_RESPONSE_TYPE = "fhir_questionnaire_response"

    def __init__(self, http_client, mds_url):
        self._http_client = http_client
        self._mds_url = mds_url

    async def get_resource(self, resource_type, resource_id, case_id, shallow=False):
        if resource_type == self._WSI_TYPE:
            return await self._get_wsi_extended_info(resource_id)
        elif resource_type in self._ANNOTATION_TYPES:
            return await self._get_annotation(resource_id, case_id)
        elif resource_type in self._PRIMITIVE_TYPES:
            return await self._get_primitive(resource_id, case_id)
        elif resource_type in self._PIXELMAP_TYPES:
            return await self._get_pixelmap(resource_id, case_id)
        elif resource_type == self._CLASS_TYPE:
            return await self._get_classification(resource_id, case_id)
        elif resource_type == self._COLLECTION_TYPE:
            data = await self._get_collection(resource_id, case_id, shallow)
            if not shallow:
                await self._resolve_wsi_items(data)
            return data
        elif resource_type == self._FHIR_QUESTIONNAIRE_TYPE:
            return await self._get_fhir_questionnaire(resource_id)
        utils.raise_http_exception(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="programming error")

    async def create_resource(self, resource_type, resource, case_id):
        if resource_type in self._ANNOTATION_TYPES:
            return await self._create_annotation(resource, case_id)
        elif resource_type in self._PRIMITIVE_TYPES:
            return await self._create_primitive(resource, case_id)
        elif resource_type in self._PIXELMAP_TYPES:
            return await self._create_pixelmap(resource, case_id)
        elif resource_type == self._CLASS_TYPE:
            return await self._create_classification(resource, case_id)
        elif resource_type == self._COLLECTION_TYPE:
            return await self._create_collection(resource, case_id)
        elif resource_type == self._FHIR_QUESTIONNAIRE_RESPONSE_TYPE:
            return await self._create_fhir_questionnaire_response(resource)
        utils.raise_http_exception(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="programming error")

    async def lock_resource(self, resource_type, resource_id, job_id, case_id):
        if resource_type in self._ANNOTATION_TYPES:
            return await self._lock_resource("annotations", resource_id, job_id, case_id)
        elif resource_type in self._PRIMITIVE_TYPES:
            return await self._lock_resource("primitives", resource_id, job_id, case_id)
        elif resource_type in self._PIXELMAP_TYPES:
            return await self._lock_resource("pixelmaps", resource_id, job_id, case_id)
        elif resource_type == self._CLASS_TYPE:
            return await self._lock_resource("classes", resource_id, job_id, case_id)
        elif resource_type == self._COLLECTION_TYPE:
            return await self._lock_resource("collections", resource_id, job_id, case_id)
        utils.raise_http_exception(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="programming error")

    async def get_job_service_public_key(self):
        case_id = None
        return await self._get_stream_resource("/v3/jobs/public-key", case_id)

    async def get_job(self, job_id):
        case_id = None
        return await self._get_resource(f"/v3/jobs/{job_id}", case_id)

    async def update_job(self, job_id, output_key, output_id):
        case_id = None
        return await self._put_stream_resource(f"/v3/jobs/{job_id}/outputs/{output_key}", {"id": output_id}, case_id)

    async def update_job_progress(self, job_id, progress):
        case_id = None
        return await self._put_stream_resource(f"/v3/jobs/{job_id}/progress", {"progress": progress}, case_id)

    async def finalize_job(self, job_id):
        case_id = None
        return await self._put_stream_resource(f"/v3/jobs/{job_id}/status", {"status": "COMPLETED"}, case_id)

    async def job_failure(self, job_id, error_message):
        case_id = None
        return await self._put_stream_resource(
            f"/v3/jobs/{job_id}/status", {"status": "ERROR", "error_message": error_message}, case_id
        )

    async def get_wsi_region_stream_response(
        self, wsi_id, level, start_x, start_y, size_x, size_y, image_format, image_quality, image_channels, z
    ):
        case_id = None
        url = f"/v3/slides/{wsi_id}/region/level/{level}/start/{start_x}/{start_y}/size/{size_x}/{size_y}"
        return await self._get_stream_resource(
            url, case_id, image_format=image_format, image_quality=image_quality, image_channels=image_channels, z=z
        )

    async def get_wsi_tile_stream_response(
        self, wsi_id, level, tile_x, tile_y, image_format, image_quality, image_channels, z
    ):
        case_id = None
        url = f"/v3/slides/{wsi_id}/tile/level/{level}/tile/{tile_x}/{tile_y}"
        return await self._get_stream_resource(
            url, case_id, image_format=image_format, image_quality=image_quality, image_channels=image_channels, z=z
        )

    async def get_wsi_download(self, wsi_id):
        case_id = None
        url = f"/v3/slides/{wsi_id}/download"
        return await self._get_stream_resource(url, case_id)

    async def get_pixelmap(self, pixelmap_id, case_id):
        return await self._get_pixelmap(pixelmap_id, case_id)

    async def get_pixelmap_info(self, pixelmap_id, case_id):
        return await self._get_pixelmap_info(pixelmap_id, case_id)

    async def update_pixelmap(self, pixelmap_id, pixelmap, case_id):
        return await self._put_stream_resource(f"/v3/pixelmaps/{pixelmap_id}", pixelmap.model_dump(), case_id)

    async def get_pixelmap_tile_stream_response(self, pixelmap_id, level, tile_x, tile_y, case_id, accept_encoding):
        url = f"/v3/pixelmaps/{pixelmap_id}/level/{level}/position/{tile_x}/{tile_y}/data"
        return await self._get_stream_resource(url, case_id, accept_encoding=accept_encoding)

    async def get_pixelmap_tiles_stream_response(
        self, pixelmap_id, level, start_x, start_y, end_x, end_y, case_id, accept_encoding
    ):
        url = f"/v3/pixelmaps/{pixelmap_id}/level/{level}/position/start/{start_x}/{start_y}/end/{end_x}/{end_y}/data"
        return await self._get_stream_resource(url, case_id, accept_encoding=accept_encoding)

    async def put_pixelmap_tile(self, pixelmap_id, level, tile_x, tile_y, data, case_id, content_encoding):
        url = f"/v3/pixelmaps/{pixelmap_id}/level/{level}/position/{tile_x}/{tile_y}/data"
        return await self._put_pixelmap_tiles(url, data, case_id, content_encoding=content_encoding)

    async def put_pixelmap_tiles(
        self, pixelmap_id, level, start_x, start_y, end_x, end_y, data, case_id, content_encoding
    ):
        url = f"/v3/pixelmaps/{pixelmap_id}/level/{level}/position/start/{start_x}/{start_y}/end/{end_x}/{end_y}/data"
        return await self._put_pixelmap_tiles(url, data, case_id, content_encoding=content_encoding)

    async def delete_pixelmap_tile(self, pixelmap_id, level, tile_x, tile_y, case_id):
        url = f"/v3/pixelmaps/{pixelmap_id}/level/{level}/tile/{tile_x}/{tile_y}/data"
        return await self._delete_stream_resource(url, case_id)

    async def query_examinations(self, query):
        case_id = None
        url = "/v3/examinations/query"
        skip = 0
        examination_list = await self._put_resource(url, query, case_id, skip=skip, limit=1000)
        while examination_list["item_count"] > 0:
            for examination in examination_list["items"]:
                yield examination
            skip = skip + examination_list["item_count"]
            examination_list = await self._put_resource(url, query, case_id, skip=skip, limit=1000)

    async def _get_annotation(self, annotation_id, case_id):
        return await self._get_stream_resource(f"/v3/annotations/{annotation_id}", case_id)

    async def _create_annotation(self, annotation, case_id):
        return await self._post_resource("/v3/annotations", annotation, case_id)

    async def _get_primitive(self, primitive_id, case_id):
        return await self._get_stream_resource(f"/v3/primitives/{primitive_id}", case_id)

    async def _create_primitive(self, primitve, case_id):
        return await self._post_resource("/v3/primitives", primitve, case_id)

    async def _get_pixelmap(self, pixelmap_id, case_id):
        return await self._get_stream_resource(f"/v3/pixelmaps/{pixelmap_id}", case_id)

    async def _get_pixelmap_info(self, pixelmap_id, case_id):
        return await self._get_stream_resource(f"/v3/pixelmaps/{pixelmap_id}/info", case_id)

    async def _create_pixelmap(self, pixelmap, case_id):
        return await self._post_resource("/v3/pixelmaps", pixelmap, case_id)

    async def _get_collection(self, collection_id, case_id, shallow):
        return await self._get_resource(f"/v3/collections/{collection_id}", case_id, shallow=shallow)

    async def _create_collection(self, collection, case_id):
        return await self._post_resource("/v3/collections", collection, case_id)

    async def _get_fhir_questionnaire(self, fhir_ids):
        ids = fhir_ids.split("||")
        if len(ids) != 2:
            utils.raise_http_exception(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="unable to identify logical Id and version Id for Questionnaire in Job",
            )
        return await self._get_resource(f"/v3/fhir/questionnaires/{ids[0]}/history/{ids[1]}", None)

    async def get_fhir_questionnaire_response(self, logical_id):
        return await self._get_resource(f"/v3/fhir/questionnaire-responses/{logical_id}", None)

    async def _create_fhir_questionnaire_response(self, questionnaire_response_resource):
        return await self._post_resource("/v3/fhir/questionnaire-responses", questionnaire_response_resource, None)

    async def put_fhir_questionnaire_response(self, logical_id, questionnaire_response_resource):
        return await self._put_resource(
            f"/v3/fhir/questionnaire-responses/{logical_id}", questionnaire_response_resource, None
        )

    async def get_selector_value(self, job_id, input_key):
        return await self._get_resource(f"/v3/jobs/{job_id}/inputs/{input_key}/selector", None)

    async def _lock_resource(self, resource_type_plural, resource_id, job_id, case_id):
        return await self._put_stream_resource(
            f"/v3/jobs/{job_id}/lock/{resource_type_plural}/{resource_id}",
            None,
            case_id,
        )

    async def get_filtered_collection(self, collection_id, query, case_id, skip, limit):
        if limit:
            filtered_collection = await self._put_stream_resource(
                f"/v3/collections/{collection_id}/items/query", query, case_id, skip=skip, limit=limit
            )
        else:
            filtered_collection = await self._put_stream_resource(
                f"/v3/collections/{collection_id}/items/query", query, case_id, skip=skip
            )
        return filtered_collection

    async def add_items_to_collection(self, collection_id, data_stream, case_id):
        return await self._post_stream_resource(f"/v3/collections/{collection_id}/items", data_stream, case_id)

    async def _get_classification(self, classification_id, case_id):
        return await self._get_stream_resource(f"/v3/classes/{classification_id}", case_id)

    async def _create_classification(self, classification, case_id):
        return await self._post_resource("/v3/classes", classification, case_id)

    async def _get_wsi_extended_info(self, wsi_id):
        wsi_info = await self._get_wsi_info(wsi_id)
        slide = await self._get_clinical_slide(wsi_id)
        wsi_info.update(tissue=slide["tissue"], stain=slide["stain"])
        return wsi_info

    async def _get_wsi_info(self, wsi_id):
        case_id = None
        return await self._get_resource(f"/v3/slides/{wsi_id}/info", case_id)

    async def _get_clinical_slide(self, wsi_id):
        case_id = None
        return await self._get_resource(f"/v3/slides/{wsi_id}", case_id)

    async def _resolve_wsi_items(self, collection):
        if collection["item_type"] == "collection":
            for sub_collection in collection["items"]:
                await self._resolve_wsi_items(sub_collection)
        elif collection["item_type"] == "wsi":
            collection["items"] = [await self._get_wsi_extended_info(id_item["id"]) for id_item in collection["items"]]

    async def _get_stream_resource(self, url, case_id, accept_encoding=None, **params):
        headers = {"Accept-Encoding": accept_encoding, "case-id": case_id}
        return await self._http_client.get_stream_response(self._mds_url + url, headers=headers, params=params)

    async def _delete_stream_resource(self, url, case_id, **params):
        headers = {"case-id": case_id}
        return await self._http_client.delete_stream_response(self._mds_url + url, headers=headers, params=params)

    async def _post_stream_resource(self, url, data_stream, case_id, **params):
        headers = {"Content-Type": "application/json", "case-id": case_id}
        return await self._http_client.post_stream_response(
            self._mds_url + url, data=data_stream, headers=headers, params=params
        )

    async def _put_stream_resource(self, url, payload, case_id, **params):
        headers = {"case-id": case_id}
        return await self._http_client.put_stream_response(
            self._mds_url + url, json=payload, headers=headers, params=params
        )

    async def _put_pixelmap_tiles(self, url, data, case_id, content_encoding):
        headers = {"Content-Type": "application/octet-stream", "Content-Encoding": content_encoding, "case-id": case_id}
        return await self._http_client.put_stream_response(
            self._mds_url + url,
            data=data,
            headers=headers,
        )

    async def _get_resource(self, url, case_id, **params):
        headers = {"case-id": case_id}
        return await self._http_client.get(self._mds_url + url, headers=headers, params=params)

    async def _post_resource(self, url, payload, case_id, **params):
        headers = {"Content-Type": "application/json", "case-id": case_id}
        return await self._http_client.post(self._mds_url + url, data=payload, params=params, headers=headers)

    async def _put_resource(self, url, payload, case_id, **params):
        headers = {"case-id": case_id}
        return await self._http_client.put(self._mds_url + url, json=payload, headers=headers, params=params)
