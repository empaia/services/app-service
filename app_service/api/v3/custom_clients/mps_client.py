class MarketplaceServiceClient:
    def __init__(self, http_client, mps_url, organization_id):
        self._http_client = http_client
        self._headers = {"organization-id": organization_id}
        self._base_url = f"{mps_url}/v1"

    async def get_configuration(self, app_id):
        url = f"{self._base_url}/customer/apps/{app_id}/config"
        return await self._http_client.get(url, headers=self._headers)

    async def get_ead(self, app_id):
        url = f"{self._base_url}/customer/apps/{app_id}"
        app = await self._http_client.get(url, headers=self._headers)
        return app["ead"]
