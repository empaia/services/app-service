from typing import Annotated

from fastapi import Header, Request
from fastapi.responses import StreamingResponse

from ..models.v3.annotation.pixelmaps import Pixelmap, PixelmapSlideInfo, UpdatePixelmap
from . import utils
from .custom_clients import mds_client
from .custom_models import params
from .custom_models.response import Message

PixelmapTileResponse = {
    200: {
        "content": {
            "application/octet-stream": {"schema": {"type": "string", "format": "binary"}},
        },
        "description": "Pixelmap tile response.",
    },
    404: {"model": Message, "description": "Pixelmap not found"},
}


def add_routes_pixelmap(app):
    @app.get(
        "/{job_id}/pixelmaps/{pixelmap_id}",
        summary="Returns a Pixelmap",
        status_code=200,
        responses={200: {"model": Pixelmap}, 404: {"model": Message, "description": "Pixelmap not found"}},
    )
    async def _(
        job_id: str = params.JobId,
        pixelmap_id: str = params.PixelmapId,
    ):
        case_id = await utils.get_case_id(job_id)
        return await mds_client.get_pixelmap(pixelmap_id, case_id)

    @app.put(
        "/{job_id}/pixelmaps/{pixelmap_id}",
        summary="Update a Pixelmap",
        status_code=200,
        responses={
            200: {"model": Pixelmap},
            404: {"model": Message, "description": "Pixelmap not found"},
            423: {"model": Message, "description": "Pixelmap is locked"},
        },
    )
    async def _(
        job_id: str = params.JobId,
        pixelmap_id: str = params.PixelmapId,
        update_pixelmap: UpdatePixelmap = params.PixelmapUpdate,
    ):
        case_id = await utils.get_case_id(job_id)
        return await mds_client.update_pixelmap(pixelmap_id, update_pixelmap, case_id)

    @app.put(
        "/{job_id}/pixelmaps/{pixelmap_id}/level/{level}/position/{tile_x}/{tile_y}/data",
        summary="Uploads data for a tile of a pixelmap",
        status_code=204,
        responses={
            204: {},
            404: {"model": Message, "description": "Pixelmap not found"},
            422: {"model": Message, "description": "Invalid parameters"},
            423: {"model": Message, "description": "Pixelmap is locked"},
        },
    )
    async def _(
        request: Request,
        job_id: str = params.JobId,
        pixelmap_id: str = params.PixelmapId,
        level: int = params.ZoomLevel,
        tile_x: int = params.TileX,
        tile_y: int = params.TileY,
        content_encoding: Annotated[str | None, Header()] = None,
    ):
        case_id = await utils.get_case_id(job_id)
        return await mds_client.put_pixelmap_tile(
            pixelmap_id, level, tile_x, tile_y, request.stream(), case_id, content_encoding=content_encoding
        )

    @app.put(
        "/{job_id}/pixelmaps/{pixelmap_id}/level/{level}/position/start/{start_x}/{start_y}/end/{end_x}/{end_y}/data",
        summary="Bulk tile upload for a pixelmap",
        status_code=204,
        responses={
            204: {},
            404: {"model": Message, "description": "Pixelmap not found"},
            413: {"model": Message, "description": "Requested data too large"},
            422: {"model": Message, "description": "Invalid parameters"},
            423: {"model": Message, "description": "Pixelmap is locked"},
        },
    )
    async def _(
        request: Request,
        job_id: str = params.JobId,
        pixelmap_id: str = params.PixelmapId,
        level: int = params.ZoomLevel,
        start_x: int = params.PixelmapStartX,
        start_y: int = params.PixelmapStartY,
        end_x: int = params.PixelmapEndX,
        end_y: int = params.PixelmapEndY,
        content_encoding: Annotated[str | None, Header()] = None,
    ):
        case_id = await utils.get_case_id(job_id)
        return await mds_client.put_pixelmap_tiles(
            pixelmap_id,
            level,
            start_x,
            start_y,
            end_x,
            end_y,
            request.stream(),
            case_id,
            content_encoding=content_encoding,
        )

    @app.get(
        "/{job_id}/pixelmaps/{pixelmap_id}/level/{level}/position/{tile_x}/{tile_y}/data",
        summary="Get the data for a tile of a pixelmap",
        status_code=200,
        responses=PixelmapTileResponse,
        response_class=StreamingResponse,
    )
    async def _(
        job_id: str = params.JobId,
        pixelmap_id: str = params.PixelmapId,
        level: int = params.ZoomLevel,
        tile_x: int = params.TileX,
        tile_y: int = params.TileY,
        accept_encoding: Annotated[str | None, Header()] = None,
    ):
        case_id = await utils.get_case_id(job_id)
        return await mds_client.get_pixelmap_tile_stream_response(
            pixelmap_id, level, tile_x, tile_y, case_id, accept_encoding=accept_encoding
        )

    @app.get(
        "/{job_id}/pixelmaps/{pixelmap_id}/level/{level}/position/start/{start_x}/{start_y}/end/{end_x}/{end_y}/data",
        summary="Bulk tile retrieval for a pixelmap",
        status_code=200,
        responses={
            200: {
                "content": {
                    "application/octet-stream": {"schema": {"type": "string", "format": "binary"}},
                },
                "description": "Pixelmap tile response.",
            },
            404: {"model": Message, "description": "Pixelmap not found"},
            413: {"model": Message, "description": "Requested data too large"},
            422: {"model": Message, "description": "Invalid parameters"},
        },
    )
    async def _(
        job_id: str = params.JobId,
        pixelmap_id: str = params.PixelmapId,
        level: int = params.ZoomLevel,
        start_x: int = params.PixelmapStartX,
        start_y: int = params.PixelmapStartY,
        end_x: int = params.PixelmapEndX,
        end_y: int = params.PixelmapEndY,
        accept_encoding: Annotated[str | None, Header()] = None,
    ):
        case_id = await utils.get_case_id(job_id)
        return await mds_client.get_pixelmap_tiles_stream_response(
            pixelmap_id, level, start_x, start_y, end_x, end_y, case_id, accept_encoding=accept_encoding
        )

    @app.delete(
        "/{job_id}/pixelmaps/{pixelmap_id}/level/{level}/position/{tile_x}/{tile_y}/data",
        summary="Delete the data for a tile of a pixelmap",
        status_code=204,
        responses={
            204: {},
            404: {"model": Message, "description": "Pixelmap not found"},
            423: {"model": Message, "description": "Pixelmap is locked"},
        },
    )
    async def _(
        job_id: str = params.JobId,
        pixelmap_id: str = params.PixelmapId,
        level: int = params.ZoomLevel,
        tile_x: int = params.TileX,
        tile_y: int = params.TileY,
    ):
        case_id = await utils.get_case_id(job_id)
        return await mds_client.delete_pixelmap_tile(pixelmap_id, level, tile_x, tile_y, case_id)

    @app.get(
        "/{job_id}/pixelmaps/{pixelmap_id}/info",
        summary="Get level and resolution of the referenced slide",
        status_code=200,
        responses={
            200: {"model": PixelmapSlideInfo},
            404: {"model": Message, "description": "Pixelmap not found or info not available"},
        },
    )
    async def _(
        job_id: str = params.JobId,
        pixelmap_id: str = params.PixelmapId,
    ):
        case_id = await utils.get_case_id(job_id)
        return await mds_client.get_pixelmap_info(pixelmap_id, case_id)
