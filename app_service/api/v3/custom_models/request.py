from __future__ import annotations as __annotations__

from enum import Enum
from typing import Union

from pydantic import Field, constr

from ...models.v3.annotation.annotations import (
    PostArrowAnnotation,
    PostCircleAnnotation,
    PostLineAnnotation,
    PostPointAnnotation,
    PostPolygonAnnotation,
    PostRectangleAnnotation,
)
from ...models.v3.annotation.classes import PostClass
from ...models.v3.annotation.collections import ItemQuery, PostCollectionApps
from ...models.v3.annotation.pixelmaps import (
    PostContinuousPixelmap,
    PostDiscretePixelmap,
    PostNominalPixelmap,
)
from ...models.v3.annotation.primitives import (
    PostBoolPrimitive,
    PostFloatPrimitive,
    PostIntegerPrimitive,
    PostStringPrimitive,
)
from ...models.v3.commons import RestrictedBaseModel
from ...models.v3.fhir.questionnaire_responses import QuestionnaireResponse
from ...models.v3.fhir.questionnaires import Questionnaire
from ...models.v3.job import PutJobProgress

# pylint unused import suppression interferes with isort / black
# workaround to mute pylint regarding this warning
assert ItemQuery
assert PutJobProgress


# Custom definition without "wsi" type in order
# to generate a correct OpenAPI specification
# for posting outputs
class CollectionItemType(str, Enum):
    INTEGER = "integer"
    FLOAT = "float"
    BOOL = "bool"
    STRING = "string"
    POINT = "point"
    LINE = "line"
    ARROW = "arrow"
    CIRCLE = "circle"
    RECTANGLE = "rectangle"
    POLYGON = "polygon"
    CLASS = "class"
    CONTINUOUS = "continuous_pixelmap"
    DISCRETE = "discrete_pixelmap"
    NOMINAL = "nominal_pixelmap"
    COLLECTION = "collection"


class ImageFormat(str, Enum):
    bmp = "bmp"
    gif = "gif"
    jpeg = "jpeg"
    png = "png"
    tiff = "tiff"


class Failure(RestrictedBaseModel):
    user_message: constr(max_length=255) = Field(..., description="A human-readable error message for the user")


AnyAnnotation = Union[
    PostPointAnnotation,
    PostLineAnnotation,
    PostArrowAnnotation,
    PostCircleAnnotation,
    PostRectangleAnnotation,
    PostPolygonAnnotation,
]

AnyPrimitive = Union[PostFloatPrimitive, PostIntegerPrimitive, PostBoolPrimitive, PostStringPrimitive]

AnyPixelmap = Union[PostContinuousPixelmap, PostDiscretePixelmap, PostNominalPixelmap]

AnyFHIRType = Union[Questionnaire, QuestionnaireResponse]

AnyOutput = Union[AnyAnnotation, AnyPrimitive, AnyPixelmap, AnyFHIRType, PostClass, PostCollectionApps]
