from fastapi import Body, Path, Query

from .request import ImageFormat

JobId = Path(
    example="29630c57-a318-43cd-ad0a-00dcbaa3055c",
    description="Id of the corresponding job submitted to the app via ENV variable",
)
InputKey = Path(example="reference-roi", description="Name of the input as specified in the EAD")
OutputKey = Path(example="nuclei-centers", description="Name of the output as specified in the EAD")
WsiId = Path(
    example="fb47ffdf-55eb-4438-b237-0a1e1948754c",
    description="Id of the whole slide image, e.g., the reference_id of an annotation",
)
ZoomLevel = Path(example=4, description="Requested level in the image pyramid")
StartX = Path(example=2341, description="X position of the region to start from")
StartY = Path(example=3421, description="Y position of the region to start from")
SizeX = Path(example=1024, description="Size of the region on the x axis")
SizeY = Path(example=1024, description="Size of the region on the y axis")
TileX = Path(example=2, description="X position of the tile at the given level")
TileY = Path(example=3421, description="Y position of the tile at the given level")
PixelmapStartX = Path(example=0, description="X start position of the tile at the given level")
PixelmapStartY = Path(example=0, description="Y start position of the tile at the given level")
PixelmapEndX = Path(example=0, description="X end position of the tile at the given level")
PixelmapEndY = Path(example=0, description="Y end position of the tile at the given level")

ImageFormat = Query(ImageFormat.png, example="png", description="Image format (defaults to png if not given)")
ImageQuality = Query(90, example=90, ge=0, le=100, description="Image quality (only considered for specific formats)")
ImageChannels = Query(None, description="List of selected channels")
ZStack = Query(0, description="Z-Stack layer index z")
Shallow = Query(
    False, example=False, description="Flag whether a given collection (collection leafs) should contain items"
)
CollectionId = Path(example="aa988c4a-fc80-4432-92fc-e7f2d664e24a", description="Id of the collection")
PixelmapId = Path(example="aa988c4a-fc80-4432-92fc-e7f2d664e24a", description="Id of the pixelmap")
Skip = Query(0, example=0, description="Optional skip parameter used for pagination")
Limit = Query(None, example=100, description="Limit the number of results used for pagination")

ItemQuery = Body(
    ...,
    example={
        "references": ["fb47ffdf-55eb-4438-b237-0a1e1948754c"],
        "viewport": {"x": 180, "y": 240, "width": 1280, "height": 1024},
    },
    description="The corresponding object used for a collection query",
)

Output = Body(..., description="The corresponding result according to the EAD specification")

PixelmapUpdate = Body(
    ...,
    example={
        "name": "Pixelmap Name",
        "description": "Pixelmap Description",
        "channel_class_mapping": [
            {"class_value": "org.empaia.my_vendor.my_app.v3.0.classes.non_tumor", "number_value": 0}
        ],
        "min_value": -10.7,
        "neutral_value": 0,
        "max_value": 42.43,
    },
    description="The corresponding object used for a pixelmap update",
)

PostItemList = Body(
    ...,
    example={
        "items": [
            {
                "type": "point",
                "name": "cell",
                "coordinates": [1234, 1234],
                "reference_id": "fb47ffdf-55eb-4438-b237-0a1e1948754c",
                "npp_created": 45.56,
            },
            {
                "type": "point",
                "name": "cell",
                "coordinates": [3112, 1411],
                "reference_id": "fb47ffdf-55eb-4438-b237-0a1e1948754c",
                "npp_created": 45.56,
            },
        ]
    },
    description="The corresponding items that should be added to the given collection",
)

Failure = Body(
    ...,
    example={"user_message": "Region of interest must be at least 256x256"},
    description="An object containing a human-readable message for the user",
)

PutJobProgress = Body(
    ...,
    example={"progress": 0.75},
    description="The progress of the job between 0 and 1",
)

LogicalId = Path(example="DABCDEFGHIJK", description="Logical Id of the FHIR Resource")
QuestionnaireResponse = Body(
    ...,
    example={
        "resourceType": "QuestionnaireResponse",
        "questionnaire": "Questionnaire/questionnaire-id",
        "status": "completed",
    },
    description="FHIR QuestionnaireResponse Resource",
)
