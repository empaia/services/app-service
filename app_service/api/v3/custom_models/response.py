from __future__ import annotations as __annotations__

from enum import Enum
from typing import Dict, List, Optional, Union

from pydantic import BaseModel, Field, StrictBool, StrictFloat, StrictInt, StrictStr

from ...models.v3.annotation.annotations import (
    ArrowAnnotation,
    CircleAnnotation,
    LineAnnotation,
    PointAnnotation,
    PolygonAnnotation,
    RectangleAnnotation,
)
from ...models.v3.annotation.classes import Class
from ...models.v3.annotation.collections import Collection as AnnotCollection
from ...models.v3.annotation.collections import ItemQueryList as AnnotItemQueryList
from ...models.v3.annotation.pixelmaps import (
    ContinuousPixelmap,
    DiscretePixelmap,
    NominalPixelmap,
)
from ...models.v3.annotation.primitives import (
    BoolPrimitive,
    FloatPrimitive,
    IntegerPrimitive,
    StringPrimitive,
)
from ...models.v3.commons import IdObject, ItemCount, Message, RestrictedBaseModel
from ...models.v3.fhir.questionnaire_responses import QuestionnaireResponse
from ...models.v3.fhir.questionnaires import Questionnaire
from ...models.v3.job import JobMode
from ...models.v3.slide import SlideInfo as TechnicalSlideInfo

# pylint unused import suppression interferes with isort / black
# workaround to mute pylint regarding this warning
assert IdObject


class SlideInfo(TechnicalSlideInfo):
    stain: Optional[str]
    tissue: Optional[str]


# Custom definition without "wsi" type in order
# to generate a correct OpenAPI specification
# for the response of posting outputs
class CollectionItemType(str, Enum):
    INTEGER = "integer"
    FLOAT = "float"
    BOOL = "bool"
    STRING = "string"
    POINT = "point"
    LINE = "line"
    ARROW = "arrow"
    CIRCLE = "circle"
    RECTANGLE = "rectangle"
    POLYGON = "polygon"
    CLASS = "class"
    CONTINUOUS = "continuous_pixelmap"
    DISCRETE = "discrete_pixelmap"
    NOMINAL = "nominal_pixelmap"
    COLLECTION = "collection"


class Collection(AnnotCollection):
    item_type: CollectionItemType
    items: Union[
        List[PointAnnotation],
        List[LineAnnotation],
        List[ArrowAnnotation],
        List[CircleAnnotation],
        List[RectangleAnnotation],
        List[PolygonAnnotation],
        List[IntegerPrimitive],
        List[FloatPrimitive],
        List[BoolPrimitive],
        List[StringPrimitive],
        List[Class],
        List[ContinuousPixelmap],
        List[DiscretePixelmap],
        List[NominalPixelmap],
        List[SlideInfo],
        List[Collection],
    ] = Field(
        description="Items belonging to the collection",
        example=[
            {
                "id": "1ccdf4c0-e3ee-4fac-adcb-8503c57dc5c9",
                "name": "ROI",
                "description": "An ROI created by some user",
                "creator_id": "b10648a7-340d-43fc-a2d9-4d91cc86f33f",
                "creator_type": "user",
                "reference_id": "fb47ffdf-55eb-4438-b237-0a1e1948754c",
                "reference_type": "wsi",
                "type": "rectangle",
                "upper_left": [100, 200],
                "width": 500,
                "height": 400,
                "npp_created": 45.56,
            },
            {
                "id": "9075456a-3500-48cb-b569-71000fb35ca6",
                "name": "ROI",
                "description": "An ROI created by some user",
                "creator_id": "b10648a7-340d-43fc-a2d9-4d91cc86f33f",
                "creator_type": "user",
                "reference_id": "fb47ffdf-55eb-4438-b237-0a1e1948754c",
                "reference_type": "wsi",
                "type": "rectangle",
                "upper_left": [500, 800],
                "width": 500,
                "height": 350,
                "npp_created": 45.56,
            },
            {
                "id": "b21a07c0-c4bf-47ac-8662-2e5a5841f393",
                "name": "ROI",
                "description": "An ROI created by some user",
                "creator_id": "b10648a7-340d-43fc-a2d9-4d91cc86f33f",
                "creator_type": "user",
                "reference_id": "fb47ffdf-55eb-4438-b237-0a1e1948754c",
                "reference_type": "wsi",
                "type": "rectangle",
                "upper_left": [1200, 1600],
                "width": 950,
                "height": 640,
                "npp_created": 45.56,
            },
        ],
    )


Collection.model_rebuild()


class ItemQueryList(AnnotItemQueryList):
    items: Union[
        List[PointAnnotation],
        List[LineAnnotation],
        List[ArrowAnnotation],
        List[CircleAnnotation],
        List[RectangleAnnotation],
        List[PolygonAnnotation],
        List[IntegerPrimitive],
        List[FloatPrimitive],
        List[BoolPrimitive],
        List[StringPrimitive],
        List[Class],
        List[ContinuousPixelmap],
        List[DiscretePixelmap],
        List[NominalPixelmap],
        List[SlideInfo],
        List[Collection],
    ] = Field(description="Items for the given query")


class NewItemsListResponse(BaseModel):
    item_count: ItemCount = Field(example=12345, description="Count of items.")
    items: Union[
        List[PointAnnotation],
        List[LineAnnotation],
        List[ArrowAnnotation],
        List[CircleAnnotation],
        List[RectangleAnnotation],
        List[PolygonAnnotation],
        List[IntegerPrimitive],
        List[FloatPrimitive],
        List[BoolPrimitive],
        List[StringPrimitive],
        List[Class],
        List[ContinuousPixelmap],
        List[DiscretePixelmap],
        List[NominalPixelmap],
        List[Collection],
    ] = Field(description="Items that were added to the collection")


NewItemsResponse = Union[Message, NewItemsListResponse]

AnyAnnotation = Union[
    PointAnnotation, LineAnnotation, ArrowAnnotation, CircleAnnotation, RectangleAnnotation, PolygonAnnotation
]

AnyPrimitive = Union[IntegerPrimitive, FloatPrimitive, BoolPrimitive, StringPrimitive]

AnyPixelmap = Union[ContinuousPixelmap, DiscretePixelmap, NominalPixelmap]

AnyFHIRType = Union[Questionnaire, QuestionnaireResponse]

AnyInput = Union[AnyAnnotation, AnyPrimitive, AnyPixelmap, AnyFHIRType, Class, SlideInfo, Collection]

AnyOutput = Union[AnyAnnotation, AnyPrimitive, AnyPixelmap, AnyFHIRType, Class, Collection]

ConfigurationSection = Dict[StrictStr, Union[StrictStr, StrictBool, StrictInt, StrictFloat]]

config_section_example = {
    "some_token": "secret-token",
    "some_flag": True,
    "some_parameter": 42,
    "some_other_param": 42.5,
}


class ConfigurationModel(RestrictedBaseModel):
    global_: ConfigurationSection = Field(default={}, alias="global", example=config_section_example)
    customer: ConfigurationSection = Field(default={}, example=config_section_example)


class JobModeResponse(RestrictedBaseModel):
    mode: JobMode = Field(..., example=JobMode.PREPROCESSING)
