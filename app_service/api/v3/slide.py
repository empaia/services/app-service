from typing import List

from . import utils
from .custom_clients import mds_client, mps_client
from .custom_models import params
from .custom_models.request import ImageFormat
from .custom_models.response import Message


def add_routes_slide(app):
    @app.get(
        "/{job_id}/regions/{wsi_id}/level/{level}/start/{start_x}/{start_y}/size/{size_x}/{size_y}",
        summary="Query WSI region",
        responses={
            200: {"content": {"image": {}}, "description": "WSI region image in given format"},
            404: {"model": Message, "description": "Slide not found"},
        },
    )
    async def _(
        job_id: str = params.JobId,
        wsi_id: str = params.WsiId,
        level: int = params.ZoomLevel,
        start_x: int = params.StartX,
        start_y: int = params.StartY,
        size_x: int = params.SizeX,
        size_y: int = params.SizeY,
        image_format: ImageFormat = params.ImageFormat,
        image_quality: int = params.ImageQuality,
        image_channels: List[int] = params.ImageChannels,
        z: int = params.ZStack,
    ):
        """
        Queries a specific region of the wsi with the given id using a zoom level, start coordinates, and a region size.
        Note that a lossless format such as PNG does NOT necessarily mean that the original data was stored lossless.
        """
        _ = job_id
        return await mds_client.get_wsi_region_stream_response(
            wsi_id, level, start_x, start_y, size_x, size_y, image_format.value, image_quality, image_channels, z
        )

    @app.get(
        "/{job_id}/tiles/{wsi_id}/level/{level}/position/{tile_x}/{tile_y}",
        summary="Query WSI tile",
        responses={
            200: {"content": {"image": {}}, "description": "WSI tile image in given format"},
            404: {"model": Message, "description": "Slide not found"},
        },
    )
    async def _(
        job_id: str = params.JobId,
        wsi_id: str = params.WsiId,
        level: int = params.ZoomLevel,
        tile_x: int = params.TileX,
        tile_y: int = params.TileY,
        image_format: ImageFormat = params.ImageFormat,
        image_quality: int = params.ImageQuality,
        image_channels: List[int] = params.ImageChannels,
        z: int = params.ZStack,
    ):
        """
        Queries a specific tile of the wsi with the given id using a zoom level and indexed position of the tile.
        Note that a lossless format such as PNG does NOT necessarily mean that the original data was stored lossless.
        """
        _ = job_id
        return await mds_client.get_wsi_tile_stream_response(
            wsi_id, level, tile_x, tile_y, image_format.value, image_quality, image_channels, z
        )

    @app.get(
        "/{job_id}/slides/{wsi_id}/download",
        summary="Download WSI file(s) as ZIP",
        responses={
            200: {"content": {"application/zip": {}}, "description": "ZIP file containing files of the given slide"},
            404: {"model": Message, "description": "Slide not found"},
        },
    )
    async def _(job_id: str = params.JobId, wsi_id: str = params.WsiId):
        """
        Queries a specific tile of the wsi with the given id using a zoom level and indexed position of the tile.
        Note that a lossless format such as PNG does NOT necessarily mean that the original data was stored lossless.
        """
        job = await mds_client.get_job(job_id)
        ead = await mps_client.get_ead(job["app_id"])
        if "permissions" in ead and ead["permissions"].get("wsi_raw_file_access", False):
            return await mds_client.get_wsi_download(wsi_id)
        utils.raise_http_exception(403, "Permission to access raw WSI file not present in EAD")
