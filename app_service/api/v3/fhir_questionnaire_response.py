from fastapi import status

from ..models.v3.fhir.questionnaire_responses import (
    PostQuestionnaireResponse,
    QuestionnaireResponse,
)
from . import utils
from .custom_clients import mds_client
from .custom_models import params
from .custom_models.response import Message


def add_routes_fhir_questionnaire_response(app):
    @app.put(
        "/{job_id}/fhir/questionnaire-responses/{logical_id}",
        summary="Update a FHIR QuestionnaireResponse Resource",
        status_code=200,
        responses={
            200: {"model": QuestionnaireResponse},
            404: {"model": Message, "description": "QuestionnaireResponse with logical_id not found"},
        },
    )
    async def _(
        job_id: str = params.JobId,
        logical_id: str = params.LogicalId,
        questionnaire_response: PostQuestionnaireResponse = params.QuestionnaireResponse,
    ):
        # ensure that logical Id of given QuestionnaireResponse object actually exists
        response = await mds_client.get_fhir_questionnaire_response(logical_id)
        if response is None:
            utils.raise_http_exception(
                status.HTTP_404_NOT_FOUND, f"Could not retrieve a QuestionnaireResponse with logical Id {logical_id}"
            )

        # ensure that provided logical Id already exists in job output
        job = await mds_client.get_job(job_id)
        output_key = None
        for output_key, output_value in job["outputs"].items():
            output_split = output_value.split("||")
            if len(output_split) == 2 and output_split[0] == logical_id:
                output_key = output_split[0]
                break

        if output_key is None:
            utils.raise_http_exception(
                status_code=status.HTTP_404_NOT_FOUND, detail="unable to identify QuestionnaireResponse in Job Output"
            )

        # put questionnaire response
        data = questionnaire_response.model_dump(by_alias=True, exclude_unset=True, exclude_none=True, mode="json")
        result = await mds_client.put_fhir_questionnaire_response(logical_id, data)
        result_id = f'{result["id"]}||{result["meta"]["versionId"]}'

        # use logical_id to identify the correct job output key
        await mds_client.update_job(job_id, output_key, result_id)
        return result
