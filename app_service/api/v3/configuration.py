from fastapi.responses import JSONResponse

from .custom_clients import mds_client, mps_client
from .custom_models import params, response


def add_routes_configuration(app):
    @app.get(
        "/{job_id}/configuration",
        summary="Get secrets configuration",
        response_model=response.ConfigurationModel,
    )
    async def _(job_id: str = params.JobId):
        """
        Queries the secrets configuration for the given app which is processing the job.
        """
        job = await mds_client.get_job(job_id)
        mps_configuration = await mps_client.get_configuration(job["app_id"])
        del mps_configuration["app_id"]
        return JSONResponse(mps_configuration, status_code=200)
