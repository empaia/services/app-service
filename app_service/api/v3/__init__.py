from .collection import add_routes_collection
from .configuration import add_routes_configuration
from .fhir_questionnaire_response import add_routes_fhir_questionnaire_response
from .input import add_routes_input
from .job import add_routes_job
from .output import add_routes_output
from .pixelmap import add_routes_pixelmap
from .slide import add_routes_slide


def add_routes_v3(app):
    add_routes_configuration(app)
    add_routes_input(app)
    add_routes_output(app)
    add_routes_collection(app)
    add_routes_slide(app)
    add_routes_pixelmap(app)
    add_routes_job(app)
    add_routes_fhir_questionnaire_response(app)
