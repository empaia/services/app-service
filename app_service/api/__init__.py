from fastapi import FastAPI, Security

from .. import __version__
from ..auth import JWTPathAuthorization
from ..singletons import settings
from .alive import add_routes_alive
from .v0 import add_routes_v0
from .v3 import add_routes_v3

if settings.enable_token_verification:

    def dependencies_creator(js_pub_key_uri):
        return [Security(JWTPathAuthorization(js_pub_key_uri, job_id="sub"))]

else:

    def dependencies_creator(js_pub_key_uri):
        return []


def add_routes(app):
    v0_app = FastAPI(
        title="App API (v0)", dependencies=dependencies_creator("/v1/job-service/public-key"), version=__version__
    )
    v3_app = FastAPI(
        title="App API (v3)", dependencies=dependencies_creator("/v3/jobs/public-key"), version=__version__
    )
    add_routes_v0(v0_app)
    add_routes_v3(v3_app)
    app.mount("/v0", v0_app)
    app.mount("/v3", v3_app)
    add_routes_alive(app)
