import asyncio
from contextlib import asynccontextmanager

from fastapi import FastAPI

from . import __version__ as version
from .api import add_routes
from .profiling import add_profiling
from .singletons import http_client, settings


@asynccontextmanager
async def lifespan(fastapi_app: FastAPI):
    asyncio.create_task(http_client.update_token_routine())
    yield


app = FastAPI(title="App Service", version=version, root_path=settings.root_path, lifespan=lifespan)
add_routes(app)

if settings.enable_profiling:
    add_profiling(app)
