from typing import Any, Dict

import jwt
from fastapi import HTTPException, Request, status
from fastapi.security import HTTPAuthorizationCredentials, HTTPBearer

from .singletons import http_client, logger, mds_url

JWTPayload = Dict[str, Any]


class JWTPathAuthorization(HTTPBearer):
    def __init__(self, js_pub_key_uri, **path_claims):
        super().__init__(bearerFormat="JWT", auto_error=False)
        self._js_pub_key_uri = js_pub_key_uri
        self._verification_key = None
        self._path_claims = path_claims

    async def __call__(self, request: Request) -> JWTPayload:
        logger.debug("Request_headers: %s", request.headers)
        if not self._verification_key:
            self._verification_key = await _get_job_service_public_key(self._js_pub_key_uri)
        authorization: HTTPAuthorizationCredentials = await super().__call__(request)
        if not authorization:
            raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="Not authenticated")
        try:
            payload = jwt.decode(authorization.credentials, key=self._verification_key, algorithms=["RS256"])
        except jwt.PyJWTError as exc:
            raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail=str(exc)) from exc
        else:
            for path_param, jwt_claim in self._path_claims.items():
                if path_param in request.path_params:
                    path_value = request.path_params[path_param]
                    if jwt_claim not in payload or payload[jwt_claim] != path_value:
                        raise HTTPException(status_code=status.HTTP_412_PRECONDITION_FAILED, detail="Permission denied")
        return payload


async def _get_job_service_public_key(js_pub_key_uri):
    url = f"{mds_url}{js_pub_key_uri}"
    return await http_client.get(url)
