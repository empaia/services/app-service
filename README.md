# App Service

The App Service implements a RESTful API for convenient input and output communication between containerized apps and the EMPAIA platform. It provides an abstraction layer between externally developed apps and the different data storage services inside the platform and performs authorization verification using signed tokens.

The API is used in combination with the corresponding EMPAIA App Description (EAD) which is part of each App. There is a [detailed documentation and reference guide](https://developer.empaia.org/) for App developers.

## How to run
The service is implemented as a Python module and can be run either locally or via Docker.

### Environment
Currently, the following environment variables are needed:
* `AS_IDP_URL`: URL to the identity provider for requesting tokens to authenticate to downstream services (e.g., https://auth-empaia-preprod.vitasystems.dev/auth/realms/empaia)
* `AS_CLIENT_ID`: Identifier for the client to be used when requesting tokens.
* `AS_CLIENT_SECRET`: Secret for the client to be used when requesting tokens.
* `AS_MDS_URL`: URL to the Medical Data Service that is being proxied by the App Service.
* `AS_MPS_URL`: URL to the Marketplace Service that is being used for secret configuration retrieval.

Optional:
* `AS_DEBUG`: Set True to enable logging level 'DEBUG' (defaults to False)
* `AS_HTTP_CLIENT_TIMEOUT`: timeout for created aiohttp clients (they dont close connections) in sec. default 300.
* `AS_REQUEST_TIMEOUT`: timeout for aiohttp requests in sec. default 300.
* `AS_CONNECTION_CHUNK_SIZE`: ...
* `AS_CONNECTION_LIMIT_PER_HOST`: ...
* `AS_ENABLE_ANNOT_CASE_DATA_PARTITIONING`: ...

### Run locally
The App Service uses poetry for dependency management. Install dependencies using `poetry install` and then start the service via uvicorn:
```
AS_IDP_URL=https://identity-provider-host/ \
AS_CLIENT_ID=aec2981e-f334-4ac2-a871-0a83261e7335 \
AS_CLIENT_SECRET=c4e65941-c8a1-465b-b7d1-a19facee6e69 \
AS_MDS_URL=https://medical-data-service.empaia.organization.local \
AS_MPS_URL=https://marketplace-service.empaia.org \
poetry run uvicorn app_service.app:app --reload
```

#### Or without token verification
```
AS_IDP_URL=https://identity-provider-host/ \
AS_CLIENT_ID=aec2981e-f334-4ac2-a871-0a83261e7335 \
AS_CLIENT_SECRET=c4e65941-c8a1-465b-b7d1-a19facee6e69 \
AS_MDS_URL=https://medical-data-service.empaia.organization.local \
AS_MPS_URL=https://marketplace-service.empaia.org \
AS_ENABLE_TOKEN_VERIFICATION=False \
poetry run uvicorn app_service.app:app
```
No Authorization header is required for accessing the endpoints then. Do not do this in production! There will be a warning printed out if the variable is set and its value is `False`.

### Run as Docker
Run the turnkey ready Docker image (from registry):
```
docker run --rm -it -e AS_IDP_URL=https://identity-provider-host/ \
                    -e AS_CLIENT_ID=aec2981e-f334-4ac2-a871-0a83261e7335 \
                    -e AS_CLIENT_SECRET=c4e65941-c8a1-465b-b7d1-a19facee6e69 \
                    -e AS_MDS_URL=https://medical-data-service.empaia.organization.local \
                    -e AS_MPS_URL=https://marketplace-service.empaia.org \
                    -e AS_DEBUG=True \
                    -p 8000:80 registry.gitlab.com/empaia/services/app-service
```

or (build locally)
```
docker build -t local-app-service-build .
docker run --rm -it -e AS_IDP_URL=https://identity-provider-host/ \
                    -e AS_CLIENT_ID=aec2981e-f334-4ac2-a871-0a83261e7335 \
                    -e AS_CLIENT_SECRET=c4e65941-c8a1-465b-b7d1-a19facee6e69 \
                    -e AS_MDS_URL=https://medical-data-service.empaia.organization.local \
                    -e AS_MPS_URL=https://marketplace-service.empaia.org \
                    -e AS_DEBUG=True \
                    -p 8000:80 local-app-service-build
```

or with `docker-compose` simply run:
```
cp sample.env .env # edit .env (see above)
docker-compose up -d --build
```

## API Documentation
There is an interactive documentation of the API available under the `/docs` endpoint and a ReDoc rendering under `/redoc`. Thus, if the service was started as described above, the API documentation is available at http://localhost:8000/docs and http://localhost:8000/redoc, respectively.

## Code Checks
Check your code before committing.

* always format code with `black` and `isort`
* check code quality using `pycodestyle` and `pylint`
* run tests with `pytest`

```bash
black app_service tests
isort app_service tests
pycodestyle app_service tests
pylint app_service tests
```

```bash
cp sample.env .env
docker-compose up --build -d
pytest tests -rx --maxfail=1
```
