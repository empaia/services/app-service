# Changelog

## 0.12.0

* extend App API functionality for FHIR Resource usage to support EMP-0105

## 0.11.3

* renovated

## 0.11.2

* renovated

## 0.11.1

* renovated

## 0.11.0

* implemented case data partitioning for EMP-0100

## 0.10.1

* renovated

## 0.10.0

* added indication and procedure tags to case model to support EMP-0105

## 0.9.0

* added pixelmap data accept-encondig and content-encoding headers to support EMP-0107
* added pixelmap info endpoint to support EMP-0108
* added pixelmap tests and fixed broken pixelmap routes

## 0.8.0

* added pixelmaps to v3 API

## 0.7.35

* updated dependencies

## 0.7.34

* updated dependencies

## 0.7.33

* updated dependencies

## 0.7.32

* updated models

## 0.7.31

* updated dependencies

## 0.7.30

* added manual test post model validation
* added performance test tests/v3/test_extend_collection_100k_items.py
  * e.g. run with(out) COMPOSE_MODELS_DISABLE_POST_VALIDATION=True
  * for me (klaus). The difference is ~33 seconds vs ~62 seconds
* include renovate-all
  * updated dependencies

## 0.7.29

* updated dependencies

## 0.7.28

* added configurable chunksize
* added connection limit per host

## 0.7.27

* pydantic 2
* sender-auth add request timeout

## 0.7.26

* updated dependencies

## 0.7.25

* updated dependencies

## 0.7.24

* updated dependencies

## 0.7.23

* updated dependencies

## 0.7.22

* updated dependencies

## 0.7.21

* updated dependencies

## 0.7.20

* updated dependencies

## 0.7.19

* updated dependencies

## 0.7.18

* updated for changed annotation service v3 post models

## 0.7.17

* renamed AS_DISABLE_POST_MODEL_VALIDATION to AS_ENABLE_POST_ITEM_LIST_MODEL_VALIDATION

## 0.7.16

* updated dependencies

## 0.7.15

* use separate job service public key routes depending on API version

## 0.7.14

* updated dependencies

## 0.7.13

* use new examinations query

## 0.7.12

* updated dependencies

## 0.7.11

* adapted jobs public-key route to new mds (v3)

## 0.7.10

* use relative submodule urls

## 0.7.9

* fix OpenAPI specification for output collections

## 0.7.8

* performance improvements

## 0.7.7

* fix v0 api configuration endpoint only return global

## 0.7.6

* add route to allow case id retrieval

## 0.7.5

* use extended slide info for apps to utilize tissue and stain

## 0.7.4

* test for permission in EAD to allow slide download

## 0.7.3

* fetch EAD from MPS instead of MDS (Job Service)

## 0.7.2

* updated dependencies

## 0.7.1

* activated streaming responses

## 0.7.0

* introduced v3 API with preprocessing features

## 0.6.0

* structure and test refactoring
* support API versioning

## 0.5.23

* Quickfix remove 'format' and 'raw_download' from api v0 SlideInfo model

## 0.5.22

* add profiling middleware

## 0.5.21

* updated dependencies
* updated models repo

## 0.5.20

* updated dependencies

## 0.5.19

* adapted for changed MDS collection query parameter

## 0.5.18

* updated dependencies

## 0.5.17

* updated dependencies

## 0.5.16

* adapted for changed MDS collection query parameter

## 0.5.15

* updated dependencies

## 0.5.14

* updated dependencies

## 0.5.13

* fix for MPS v1 organization header
* updated dependencies

## 0.5.12

* updated MPS v1 configuration route
* updated dependancies

## 0.5.11

* use common service status model for /alive

## 0.5.10

* updated dependencies

## 0.5.9

* update sss/vault mock to mps mock v1

## 0.5.8

* updated dependencies

## 0.5.7

* updated dependencies

## 0.5.6

* changed response codes in auth.py

## 0.5.5

* updated dependencies

## 0.5.4

* using updated python-base image

## 0.5.3

* fixed missing dependency idna

## 0.5.2

* updated ci
* added pycodestyle

## 0.5.1

* fix token decoding

## 0.5.0

* add failure endpoint
* updated libraries to current versions

## 0.4.18

* updated models submodule to include creator_type: scope

## 0.4.17

* removed timestamps form logging

## 0.4.16

* vault url is now `v0`

## 0.4.15

* fixed error due to changes to loggin (0.4.14)
* refactored Dockerfile

## 0.4.14

* refactored logging
* added env `AS_ROOT_PATH`
* added env `AS_LOG_LEVEL`
* refactored env var naming `EMPAIA_*`-> `AS_*`

## 0.4.13

* adapted to use correct payload spec of Vault Service

## 0.4.12

* updated submodules

## 0.4.11

* updated sender auth

## 0.4.10

* updated sender auth

## 0.4.9

* updated sender auth submodule which now supports proxy

## 0.4.8

* MDS API version is configured in code and not part of the MDS_URL parameter anymore

## 0.4.7

* README changes on main requires version bump

## 0.4.6

* refactor pylint configuration

## 0.4.5

* update sender-auth submodule

## 0.4.4

* update models for custom validation of reference_type

## 0.4.3

* refactored for empaia-sender-auth update

## 0.4.2

* updated sender-auth submodule to fix status-code error handling

## 0.4.1

* updated sender-auth submodule to fix potential broken-pipe http client errors when connections are reused

## 0.4.0

* updated for new models and MDS API

## 0.3.8

* updated lock routes to match new MDS ( /DADS) routes syntax

## 0.3.7

* added documentation for the `/alive` health check endpoint

## 0.3.6

* adjusted response model `AnyOutput` to include `ShallowCollection` (needed by changes from 0.3.5)

## 0.3.5

* post output endpoint now forwards the response of the post instead of a new GET for the just created resource

## 0.3.4

* fixed ImageFormat enum missing `.value`

## 0.3.2

* add optional parameter image_channels for wsi-service

## 0.3.1

* bugfix posting collection of classes
* bugfix extend nested collections

## 0.3.0

* add support for extending any collection that is owned by the given job
* allow to extend nested collections

## 0.2.0

* removed validation of reference_id
* changed output request models to be DADS core models
  * reference_type needs to be set by client when posting outputs with reference_id

## 0.1.6

* updated annotation models
  * including centroid (optional to be sent by app for post, returned as response from post, returned as response from GET / PUT query)

## 0.1.3

* added ENV `EMPAIA_APP_SERVICE_TIMEOUT` with defualt 300s for sending aiohttp requests via senderAuth repo

## 0.1.2

* locking at finalize

## 0.1.1

* added ci versioning
